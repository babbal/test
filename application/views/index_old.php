﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Alifailaan Api V1 Documentation</title>
        
        <link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url();?>docs/css/style.css"/>
		<link rel="stylesheet" href="<?php echo base_url();?>docs/css/prettify.css"/>

        <style>
            table {
                border-collapse: collapse;
                width: 100%;
                margin: 0 0 20px 0;
            }
            table {
                max-width: 100%;
                background-color: transparent;
                border-collapse: collapse;
                border-spacing: 0;
            }
            thead {
                display: table-header-group;
                vertical-align: middle;
                border-color: inherit;
            }
            tbody {
                display: table-row-group;
                vertical-align: middle;
                border-color: inherit;
            }
            tr {
                display: table-row;
                vertical-align: inherit;
                border-color: inherit;
            }
            th {
                background-color: #f5f5f5;
                text-align: left;
                font-family: "Source Sans Pro", sans-serif;
                font-weight: 700;
                padding: 4px 8px;
                border: #e0e0e0 1px solid;
            }
            td, th {
                display: table-cell;
                vertical-align: inherit;
            }
            th, td {
                font-family: "Source Sans Pro", sans-serif;
                font-weight: 400;
                font-size: 16px;
            }
            td {
                vertical-align: top;
                padding: 2px 8px;
                border: #e0e0e0 1px solid;
            }
            td.code {
                font-size: 14px;
                font-family: "Source Code Pro";
                font-style: normal;
                font-weight: 400;
            }
            td .description {
                color: #808080;
            }

            .highlight{
                border: 1px solid #e1e4e5;
                padding: 0px;
                overflow-x: auto;
                background: #fff;
                margin-bottom: 5px;
            }

            .highlight pre {
                white-space: pre;
                margin: 0;
                padding: 12px 12px;
                font-family: Consolas,"Andale Mono WT","Andale Mono","Lucida Console","Lucida Sans Typewriter","DejaVu Sans Mono","Bitstream Vera Sans Mono","Liberation Mono","Nimbus Mono L",Monaco,"Courier New",Courier,monospace;
                font-size: 12px;
                line-height: 1.5;
                display: block;
                overflow: auto;
                color: #404040;
            }
            .highlight .url_title{
                float: left;
                background: #F2D600;
            }
            .highlight .url{
                float: left;
            }
            .highlight .method_type{
                float: right;
                background: #61BD4F;
            }


            .cont{
                border-bottom: 1px solid #f5f5f5;
                padding-bottom: 5px;
            }
        </style>

    </head>
    <body>
        <div class="wrapper">


            <nav style="border-bottom: 5px solid #000;">

                <div class="pull-left">
                    <h1>
                        <a href="<?php echo base_url();?>">
                          <h1 style="font-size: 50px;"><strong>Alifailaan</strong></h1>
                        </a>
                    </h1>
                </div>
            </nav>
            <section>
                <div class="container">
                    <ul class="docs-nav ">
                        <li><strong>Getting Started</strong></li>
                        <li><a href="#welcome" class="cc-active">Welcome</a></li>
                        <!--                        <li class="separator"></li>-->
                        <li><strong>Authentication</strong></li>
						<li><a href="#login" class="cc-active">Authentification/SignUp</a></li>
						<li><a href="#specific_party" class="cc-active">Authentification/SignIn</a></li>
                        
						 
                        <li><strong>Channels </strong></li>
                        <li><a href="#countries" class="cc-active">Channels List</a></li>
						<li><a href="#live" class="cc-active">Live Channels List</a></li>
						<li><a href="#mychannel" class="cc-active">My Channel List</a></li>
						<li><a href="#user_info" class="cc-active">Create Channel</a></li>
						<li><a href="#delete_info" class="cc-active">Delete Channel</a></li>
						<li><a href="#channel_information" class="cc-active">Channel Information</a></li>
						<li><a href="#timestamp" class="cc-active">TimeStamp</a></li>
						<li><a href="#get_catsubcat" class="cc-active">Categoty && Sub Category</a></li>
						
						 <li><strong>Account</strong></li>
                        <li><a href="#profile" class="cc-active">Update Profile</a></li>
						
                    </ul>
                    <div class="docs-content">
                        <h2> Getting Started</h2>
                        <div id="welcome" class="cont">
                            <h3> Welcome</h3>
                            <p> Alifailaan Documentations </p>

                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?></pre>
                                <pre class="method_type">GET</pre>
                            </div>
                        </div>
                          <div id="login" class="cont">
                            <h3>Authentification/SignUp</h3>
                            <ul>
                                <li>Authentification/SignUp</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/signup</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								<tr>
								<td>email</td>
								<td>Required</td>
								</tr>
								
								<tr>
								<td>password</td>
								<td>Required</td>
								</tr>
								
								
								
								
								</table>
                            <pre class="prettyprint">
{  
"ERROR": false,
"ERROR_INFO": "Signup Successfully"
}							
							
							</pre>

                        </div>
                         
                        <div id="specific_party" class="cont">
                            <h3> Authentification/SignIn</h3>
                            <ul>
                                <li>Authentification/SignIn</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/signin</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>email</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>password</td>
								<td>Required</td>
								</tr>
								</table>
                            </div>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "ERROR_INFO": "Login Successfully",
  "USER": {
    "USER_ID": "100",
    "FIRSTNAME": "ahsan ",
    "LASTNAME": "admin",
    "EMAIL": "ahsan.haq511@gmail.com",
    "CURRENT_PACKAGE": "0"
  }
}
							
							</pre>
                        </div>
						
                        <div id="countries" class="cont">
                            <h3> Channels List</h3>
                            <ul>
                                <li>Channels List</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/channels</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "CHANNEL": [
    {
      "CHANNEL_ID": "25",
      "CHANNEL_NAME": "babbali",
      "CHANNEL_SLUG": "babbali",
      "CHANNEL_LOGO": "579614821589af778ebbaa.jpg",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/babbali/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-09 05:12:13"
    },
    {
      "CHANNEL_ID": "16",
      "CHANNEL_NAME": "newch",
      "CHANNEL_SLUG": "newch",
      "CHANNEL_LOGO": "1699444067589af78c4c682.jpg",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/newch/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-13 02:56:43"
    }
  ]
}

							
							</pre>

                        </div>
						
						<div id="live" class="cont">
                            <h3> Live Channels List</h3>
                            <ul>
                                <li>Live Channels List</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/live</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "CHANNEL": [
    {
      "CHANNEL_ID": "25",
      "CHANNEL_NAME": "babbali",
      "CHANNEL_SLUG": "babbali",
      "CHANNEL_LOGO": "579614821589af778ebbaa.jpg",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/babbali/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-09 05:12:13"
    },
    {
      "CHANNEL_ID": "16",
      "CHANNEL_NAME": "newch",
      "CHANNEL_SLUG": "newch",
      "CHANNEL_LOGO": "1699444067589af78c4c682.jpg",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/newch/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-13 02:56:43"
    }
  ]
}

							
							</pre>

                        </div>
						
						
					<div id="mychannel" class="cont">
                            <h3> My Channels List</h3>
                            <ul>
                                <li>My Channels List</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/mychannels</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "CHANNEL": [
    {
      "CHANNEL_ID": "1",
      "CHANNEL_NAME": "channel1",
      "CHANNEL_SLUG": "channel-1",
      "CHANNEL_LOGO": "",
      "CHANNEL_NAME_PUBLISHING": "channel-1",
      "CHANNEL_PASSWORD": "d7315e35-3012-4240-9147-dc12d039c0b8",
      "CHANNEL_PUBLISHING": "rtmp://148.251.119.156:1935/ezespn/channel-1",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/channel-1/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-15 05:31:18"
    },
    {
      "CHANNEL_ID": "2",
      "CHANNEL_NAME": "channel 2",
      "CHANNEL_SLUG": "channel-2",
      "CHANNEL_LOGO": "",
      "CHANNEL_NAME_PUBLISHING": "channel-2",
      "CHANNEL_PASSWORD": "50e75152-f50d-44aa-81a5-073fcebaa329",
      "CHANNEL_PUBLISHING": "rtmp://148.251.119.156:1935/ezespn/channel-2",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/channel-2/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-15 05:43:54"
    },
    
  ]
}
			
			
							</pre>

                        </div>	
						
						
						
						  
						
						
						<div id="user_info" class="cont">
                            <h3> Create Channel</h3>
                            <ul>
                                <li> Create Channel</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/create_channel</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>channel_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>cat_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>sub_cat_id</td>
								<td>Required</td>
								</tr>
								
								</table>
							 <pre class="prettyprint">
{
  "ERROR": false,
  "ERROR_INFO": "Channel Created Successfully"
}
							 
							 </pre>

                        </div>
						<div id="delete_info" class="cont">
                            <h3> Delete Channel</h3>
                            <ul>
                                <li> Delete Channel</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/delete_channel</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>channel_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>password</td>
								<td>Required</td>
								</tr>
								
								
								</table>
							 <pre class="prettyprint">
{
  "ERROR": false,
  "ERROR_INFO": "Channel Deleted Successfully"
}
							 
							 </pre>

                        </div>
						
						<div id="channel_information" class="cont">
                            <h3>Channel Information</h3>
                            <ul>
                                <li> Channel Information</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/channel_data</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>slug</td>
								<td>Required</td>
								</tr>
								
								
								
								</table>
							 <pre class="prettyprint">
{
  "ERROR": false,
  "ALL_CHANNELS": [
    {
      "CHANNEL_ID": "2",
      "CHANNEL_NAME": "channel 2",
      "CHANNEL_SLUG": "channel-2",
      "CHANNEL_LOGO": "thumbnail-ezespn.png",
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/channel-2/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-24 01:56:46"
    },
    {
      "CHANNEL_ID": "3",
      "CHANNEL_NAME": "channel 3",
      "CHANNEL_SLUG": "channel-3",
      "CHANNEL_LOGO": null,
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/channel-3/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-03-03 00:56:12"
    },
    {
      "CHANNEL_ID": "8",
      "CHANNEL_NAME": "Usama Test",
      "CHANNEL_SLUG": "usama-test",
      "CHANNEL_LOGO": null,
      "CHANNEL_URL": "http://148.251.119.156:1935/ezespn/_definst_/usama-test/playlist.m3u8",
      "CHANNEL_CREATED_BY": "1",
      "CHANNEL_CREATED": "2017-02-24 01:56:55"
    }
  ],
  "ABOUT": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.",
  "VIDEOS": [
    {
      "VIDEO_ID": "19",
      "VIDEO_TITLE": "channel1_2017-03-02",
      "VIDEO_DESCRIPTION": "Stream_2017-03-02",
      "VIDEO_URL": "http://148.251.119.156:1935/ezespnVod/_definst_/mp4:channel-1/channel-11488455856421.mp4/playlist.m3u8",
      "VIDEO_THUMBNAIL": "channel-11488455856421img.jpg",
      "VIDEO_VIEW": "2"
    },
    {
      "VIDEO_ID": "20",
      "VIDEO_TITLE": "babbal",
      "VIDEO_DESCRIPTION": "ahsan",
      "VIDEO_URL": "http://148.251.119.156:1935/ezespnVod/_definst_/mp4:channel-1/96189375458b9041dd0e3c.mp4/playlist.m3u8",
      "VIDEO_THUMBNAIL": "96189375458b9041dd0e3c.jpg",
      "VIDEO_VIEW": "0"
    }
  ]
}
							 
							 </pre>

                        </div>
						
						<div id="timestamp" class="cont">
                            <h3> Timestamp</h3>
                            <ul>
                                <li>Timestamp</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/timestamp</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>slug</td>
								<td>Required</td>
								</tr>
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "TIMESTAMP": 1489391272
}

							
							</pre>

                        </div>
                       
					   
					   <div id="get_catsubcat" class="cont">
                            <h3> Category && Sub Category</h3>
                            <ul>
                                <li> Category && Sub Category</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/get_catsubcat</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								
								
								</table>
							 <pre class="prettyprint">
{
  "ERROR": false,
  "CATEGORY": [
    {
      "CAT_ID": "1",
      "NAME": "Entertainment"
    },
    {
      "CAT_ID": "2",
      "NAME": "Sports"
    },
    {
      "CAT_ID": "3",
      "NAME": "Pets, Animals"
    },
   ],
  "SUB_CATEGORY": [
    {
      "SUB_CAT_ID": "1",
      "SUB_NAME": "Radio",
      "PARENT_ID": "1"
    },
    {
      "SUB_CAT_ID": "2",
      "SUB_NAME": "Other Entertainment",
      "PARENT_ID": "1"
    },
    {
      "SUB_CAT_ID": "3",
      "SUB_NAME": "How-to",
      "PARENT_ID": "1"
    },
    
  ]
}
							 
							 </pre>

                        </div>
					   
						<div id="profile" class="cont">
                            <h3>Update Profile</h3>
                            <ul>
                                <li>Update Profile</li>                            
                            </ul>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>users/updateprofile</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>first_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>last_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>email</td>
								<td>Required</td>
								</tr>
								
								
								
								
								</table>
                            <pre class="prettyprint">
{
  "ERROR": false,
  "ERROR_INFO": "Updated",
  "USER": {
    "USER_ID": "1",
    "FIRSTNAME": "babbal",
    "LASTNAME": "kalo",
    "EMAIL": "ahsan.haq511@gmail.com"
  }
}							
							
							</pre>

                        </div>

                    </div>
                </div>
            </section>

            <footer>
                <div class="">
                    <p> &copy; Copyright Ezespn. All Rights Reserved.</p>
                </div>
            </footer>
        </div>

       
		 <script src="<?php echo base_url();?>docs/js/jquery.min.js"></script>
		 <script src="<?php echo base_url();?>docs/js/prettify/prettify.js"></script>
		<link rel="stylesheet" href=""/>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&skin=sunburst"></script>
		<script src="<?php echo base_url();?>docs/js/layout.js"></script>
       

        <script>
$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
        </script>
    </body>
</html>
