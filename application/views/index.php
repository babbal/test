﻿<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="description" content="">
        <meta name="HandheldFriendly" content="True">
        <meta name="MobileOptimized" content="320">
        <meta name="viewport" content="initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <title>Alifailaan Api V1 Documentation</title>
        
        <link href="http://fonts.googleapis.com/css?family=Raleway:700,300" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?php echo base_url();?>docs/css/style.css"/>
		<link rel="stylesheet" href="<?php echo base_url();?>docs/css/prettify.css"/>

        <style>
            table {
                border-collapse: collapse;
                width: 100%;
                margin: 0 0 20px 0;
            }
            table {
                max-width: 100%;
                background-color: transparent;
                border-collapse: collapse;
                border-spacing: 0;
            }
            thead {
                display: table-header-group;
                vertical-align: middle;
                border-color: inherit;
            }
            tbody {
                display: table-row-group;
                vertical-align: middle;
                border-color: inherit;
            }
            tr {
                display: table-row;
                vertical-align: inherit;
                border-color: inherit;
            }
            th {
                background-color: #f5f5f5;
                text-align: left;
                font-family: "Source Sans Pro", sans-serif;
                font-weight: 700;
                padding: 4px 8px;
                border: #e0e0e0 1px solid;
            }
            td, th {
                display: table-cell;
                vertical-align: inherit;
            }
            th, td {
                font-family: "Source Sans Pro", sans-serif;
                font-weight: 400;
                font-size: 16px;
            }
            td {
                vertical-align: top;
                padding: 2px 8px;
                border: #e0e0e0 1px solid;
            }
            td.code {
                font-size: 14px;
                font-family: "Source Code Pro";
                font-style: normal;
                font-weight: 400;
            }
            td .description {
                color: #808080;
            }

            .highlight{
                border: 1px solid #e1e4e5;
                padding: 0px;
                overflow-x: auto;
                background: #fff;
                margin-bottom: 5px;
            }

            .highlight pre {
                white-space: pre;
                margin: 0;
                padding: 12px 12px;
                font-family: Consolas,"Andale Mono WT","Andale Mono","Lucida Console","Lucida Sans Typewriter","DejaVu Sans Mono","Bitstream Vera Sans Mono","Liberation Mono","Nimbus Mono L",Monaco,"Courier New",Courier,monospace;
                font-size: 12px;
                line-height: 1.5;
                display: block;
                overflow: auto;
                color: #404040;
            }
            .highlight .url_title{
                float: left;
                background: #F2D600;
            }
            .highlight .url{
                float: left;
            }
            .highlight .method_type{
                float: right;
                background: #61BD4F;
            }


            .cont{
                border-bottom: 1px solid #f5f5f5;
                padding-bottom: 5px;
            }
        </style>

    </head>
    <body>
        <div class="wrapper">


            <nav style="border-bottom: 5px solid #000;">

                <div class="pull-left">
                    <h1>
                        <a href="<?php echo base_url();?>">
                          <h1 style="font-size: 50px;"><strong>Alifailaan</strong></h1>
                        </a>
                    </h1>
                </div>
            </nav>
            <section>
                <div class="container">
                    <ul class="docs-nav ">
                        <li><strong>Getting Started</strong></li>
                        <li><a href="#welcome" class="cc-active">Welcome</a></li>
                        <!--                        <li class="separator"></li>-->
                        <li><a href="#login" class="cc-active">Authentification/SignUp</a></li>
						<li><a href="#specific_party" class="cc-active">Authentification/SignIn</a></li>
						<li><a href="#updateuser" class="cc-active">Update User</a></li>
						<li><a href="#mychannel" class="cc-active">List Issues</a></li>
						<li><a href="#get_catsubcat" class="cc-active">List Popular Issues</a></li>
						<li><a href="#popular" class="cc-active">Get Popular Issues</a></li>
						<li><a href="#delete_info" class="cc-active">Issue Detail</a></li>
						<li><a href="#timestamp" class="cc-active">Create Issue</a></li>
						<li><a href="#uploadmedia" class="cc-active">Upload Media</a></li>
						<li><a href="#getcomment" class="cc-active">Get Comment</a></li>
						<li><a href="#addcomment" class="cc-active">Add Comments</a></li>
						<li><a href="#user_info" class="cc-active">Update View Count</a></li>
						<li><a href="#channel_information" class="cc-active">Update Share Count</a></li>
						<li><a href="#updatepoint" class="cc-active">Update Points</a></li>
						<li><a href="#getpoint" class="cc-active">Get Points</a></li>
						<li><a href="#getreport" class="cc-active">Get my Reports</a></li>
						<li><a href="#leaderboard" class="cc-active">Leaderboard</a></li>
						
						
						
                    </ul>
                    <div class="docs-content">
                        <h2> Getting Started</h2>
                        <div id="welcome" class="cont">
                            <h3> Welcome</h3>
                            <p> Alifailaan Documentations </p>

                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?></pre>
                                <pre class="method_type">GET</pre>
                            </div>
                        </div>
                          <div id="login" class="cont">
                            <h3>Authentification/SignUp</h3>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/signup</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								<tr>
								<td>email</td>
								<td>Required</td>
								</tr>
								
								<tr>
								<td>password</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>social_media_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>sm_type</td>
								<td>Required</td>
								</tr>
								
								<tr>
								<td>device_token</td>
								<td>Required</td>
								</tr>						
								</table>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Signup Successfully",
    "data": {
        "id": "74",
        "firstname": null,
        "lastname": null,
        "email": "tester4@gmail.com",
        "phone": null,
        "social_media_id": null,
        "device_token": "1234567890",
        "sm_type": "twitter",
        "time": "2018-03-21 11:56:45"
    }
}						
							
							</pre>

                        </div>
                         
                        <div id="specific_party" class="cont">
                            <h3> Authentification/SignIn</h3>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/signin</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>email</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>password</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>phone</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>social media id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>sm_type</td>
								<td>Required</td>
								</tr>
								</table>
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Login Successfully",
    "data": {
        "id": "48",
        "firstname": null,
        "lastname": null,
        "email": "abcd@gmail.com",
        "phone": null,
        "social_media_id": null,
        "device_token": "0987654321",
        "sm_type": null,
        "time": "2018-03-15 11:08:51"
    }
}
							
							</pre>
                        </div>
						 
						
					<div id="updateuser" class="cont">
                            <h3>Update User</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/update_user</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>isActive</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>location</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>username</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>first_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>last_name</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Updated Successfully",
    "data": {
        "id": "35",
        "username": "testuser",
        "location": "Karachi",
        "time": "2018-02-21 13:11:07"
    },
    "points": 0
}
			
			
							</pre>

                        </div>	
						
						
					<div id="mychannel" class="cont">
                            <h3>List Issues</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/issues_list</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td>location</td>
								<td>Required</td>
								</tr>
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Record",
    "data": [
        {
            "issue_id": "3",
            "issue_name": "Lack of basic infrastructure",
            "issue_category": "Gender",
            "text": "40 per cent of the educational institutions don’t have basic facilities such as electricity, water, guards, toilet and boundary wall. Even worse, they don’t have tables, chairs and even black boards. It is this lack of infrastructure that makes these students easy prey for religious fundamentalist to enroll and teach them all sorts of hate",
            "status": "1",
            "location": "Harley Street, Rawalpindi, Punjab, Pakistan",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        }
    ],
    "total_pages": 0
}
			
			
							</pre>

                        </div>
						<div id="get_catsubcat" class="cont">
                            <h3>List Popular Isssues</h3>
                            
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_popular_issues</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>location</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>page_no</td>
								<td>Required</td>
								</tr>
								
								
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Record",
    "data": [
        {
            "issue_id": "2",
            "issue_name": "Education: the major problem in Pakistan",
            "issue_category": "Gender",
            "text": "Education is the most essential ingredient for the development of a nation. It is a global fact that nations who have reached the heights of the development and prosperity have done it by using education and information as a tool to do it. Education is simply an investment to achieve both human and economic development. But unfortunately it is one of the biggest problems in Pakistan. The education system of Pakistan is rotten to the core. The literacy rate in Pakistan is recorded as about 55% in 2012-2013.\n\nAt present, the education sector is facing crisis just like other major sectors of the country. Following are the major problems in education system of Pakistan:\n\nThe educational system of the country is based on different mediums which divides the students in two segments. Mostly private schools are English medium while the Government schools are Urdu medium. Regional differences are also a major cause for a poor educational system in Pakistan.\n\nGender discrimination is very common in Pakistan. People are very conservative and they don’t want their girls to go to school and get education. Due to this, the ratio of boys and girls in primary schools is 10:4.\n\nIn Human Development Report Pakistan is placed at 136th position for having just 49.9% educated population. In addition to that, Pakistan is ranked at 113th out of 120 registered UN members according to the research conducted by UNESCO et al. Some of the very basic flaws of the education system in Pakistan contribute to the economic, ethnic and sociopolitical crisis within the country.\n\nMoreover, the quality of education in most of the public schools and colleges is well below par; the teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation. Quality of teaching needs special attention in rural areas where the teachers lack in all departments.\n\nPoverty is another factor that prohibits the parents to send their children to private school where the quality of education is high. Poor people get their children admitted to Government schools. The teachers in Government schools are not professionally trained. They even don’t take lectures and waste the precious time of the students.\n\nThe allocation of funds for education sector by the Government of Pakistan are very low; only 2% of the total GDP. Government should increase this rate to improve the quality of educational system.\n\nEducation is very important for the development of every country. It is the only cure for the disability of a country because today’s students are tomorrow’s nation builder. Government of Pakistan should take steps to remove the above mentioned problems so as to improve the quality of educational system of the country.",
            "status": "1",
            "location": "Lahore Garrison University, Lahore",
            "views": "23",
            "share": "17",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "1",
            "issue_name": "Teachers of Peshawar girls’ school concerned at lack of security",
            "issue_category": "Education",
            "text": "The parents and teachers of the Government Girls Primary School Wazirbagh-I have expressed concern over the lack of proper security arrangement at the school. \nThere were two watchmen. One of which, Kamran Shahzad, had been transferred to the Government Girls Primary School Wazirbagh No-II. The strength of students at that school was far less than Wazirbagh No-I. The other watchman, Muhammad Wali, had some health problem and was facing difficulties in performing his duties. The watchman Muhammad Wali told this scribe that the school was situated in a sensitive area where the police had conducted raids in the past to arrest militants. He said that he had not been provided gun and his job became difficult so he left the job.\nGovernment of KP should take immediate notice and provide the school with armed guards for the safety of children.",
            "status": "1",
            "location": "Government Girls Primary School Wazirbagh No-I",
            "views": "7",
            "share": "13",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "3",
            "issue_name": "Lack of basic infrastructure",
            "issue_category": "Gender",
            "text": "40 per cent of the educational institutions don’t have basic facilities such as electricity, water, guards, toilet and boundary wall. Even worse, they don’t have tables, chairs and even black boards. It is this lack of infrastructure that makes these students easy prey for religious fundamentalist to enroll and teach them all sorts of hate",
            "status": "1",
            "location": "Harley Street, Rawalpindi, Punjab, Pakistan",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "4",
            "issue_name": "HOW TO IMPROVE EDUCATION SYSTEM IN PAKISTAN?\n",
            "issue_category": "Policy",
            "text": "Taking into account the flaws of education, it becomes evident that there haven’t been many solid initiatives of improvement towards obtaining any improvements until last few years when some private institutions emerged into the limelight. The history of education shows that there were not many educational facilities available to the Pakistani students who wanted to pursue college education until Punjab Colleges began to operate.",
            "status": "1",
            "location": "Faisalabad",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "5",
            "issue_name": "LOREM IPSUM dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",
            "issue_category": "Infrastructure",
            "text": "It is mandated in the Constitution of Pakistan to provide free and compulsory education to all children between the ages of 5-16 years and enhance adult literacy. With the 18th constitutional amendment the concurrent list which comprised of 47 subjects was abolished and these subjects, including education, were transferred to federating units as a move towards provincial autonomy.\n\nThe year 2015 is important in the context that it marks the deadline for the participants of Dakar declaration (Education For All [EFA] commitment) including Pakistan. Education related statistics coupled with Pakistan’s progress regarding education targets set in Vision 2030 and Pakistan’s lagging behind in achieving EFA targets and its Millennium Development Goals(MDGs) for education call for an analysis of the education system of Pakistan and to look into the issues and problems it is facing so that workable solutions could be recommended.",
            "status": "1",
            "location": "Gujranwala",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "6",
            "issue_name": "LOREM IPSUM dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa.",
            "issue_category": "Infrastructure",
            "text": "The system of education includes all institutions that are involved in delivering formal education (public and private, for-profit and nonprofit, onsite or virtual instruction) and their faculties, students, physical infrastructure, resources and rules. In a broader definition the system also includes the institutions that are directly involved in financing, managing, operating or regulating such institutions (like government ministries and regulatory bodies, central testing organizations, textbook boards and accreditation boards). The rules and regulations that guide the individual and institutional interactions within the set up are also part of the education system.",
            "status": "1",
            "location": "Peshawar",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "7",
            "issue_name": "Education system of Pakistan",
            "issue_category": "Gender",
            "text": "The education system of Pakistan is comprised of 260,903 institutions and is facilitating 41,018,384 students with the help of 1,535,461 teachers. The system includes 180,846 public institutions and 80,057 private institutions. Hence 31% educational institutes are run by private sector while 69% are public institutes.",
            "status": "1",
            "location": "Multan",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "8",
            "issue_name": "Analysis of education system in Pakistan",
            "issue_category": "Education",
            "text": "Pakistan has expressed its commitment to promote education and literacy in the country by education policies at domestic level and getting involved into international commitments on education. In this regard national education policies are the visions which suggest strategies to increase literacy rate, capacity building, and enhance facilities in the schools and educational institutes. MDGs and EFA programmes are global commitments of Pakistan for the promotion of literacy.",
            "status": "1",
            "location": "Hyderabad",
            "views": "5",
            "share": "7",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-01-30 11:18:06"
        },
        {
            "issue_id": "230",
            "issue_name": "MDGs and Pakistan",
            "issue_category": "Infrastructure",
            "text": "Due to the problems in education system of Pakistan, the country is lagging behind in achieving its MDGs of education. The MDGs have laid down two goals for education sector:\n\n \n\nGoal 2: The goal 2 of MDGs is to achieve Universal Primary Education (UPE) and by 2015, children everywhere, boys and girls alike, will be able to complete a full course of primary schooling. By the year 2014 the enrolment statistics show an increase in the enrolment of students of the age of 3-16 year while dropout rate decreased. But the need for increasing enrolment of students remains high to achieve MDGs target. Punjab is leading province wise in net primary enrolment rate with 62% enrolment. The enrolment rate in Sindh province is 52%, in Khyber Pakhtunkhawa (KPK) 54% and primary enrolment rate in Balochistan is 45%.\n\n \n\nGoal 3: The goal 3 of MDGs is Promoting Gender Equality and Women Empowerment. It is aimed at eliminating gender disparity in primary and secondary education by 2005 and in all levels of education not later than 2015. There is a stark disparity between male and female literacy rates. The national literacy rate of male was 71% while that of female was 48% in 2012-13. Provinces reported the same gender disparity. Punjab literacy rate in male was 71% and for females it was 54%. In Sindh literacy rate in male was 72% and female 47%, in KPK male 70% and females 35%, while in Balochistan male 62% and female 23%.",
            "status": "1",
            "location": "Karachi",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-02-21 13:02:40"
        },
        {
            "issue_id": "231",
            "issue_name": "Education for All (EFA) Commitment",
            "issue_category": "Policy",
            "text": "The EFA goals focus on early childhood care and education including pre-schooling, universal primary education and secondary education to youth, adult literacy with gender parity and quality of education as crosscutting thematic and programme priorities.\n\nEFA Review Report October 2014 outlines that despite repeated policy commitments, primary education in Pakistan is lagging behind in achieving its target of universal primary education. Currently the primary gross enrolment rate stands at 85.9% while Pakistan requires increasing it up to 100% by 2015-16 to fulfil EFA goals.  Of the estimated total primary school going 21.4 million children of ages 5-9 years, 68.5% are enrolled in schools, of which 8.2 million or 56% are boys and 6.5 million or 44% are girls. Economic Survey of Pakistan confirms that during the year 2013-14 literacy remained much higher in urban areas than in rural areas and higher among males.",
            "status": "1",
            "location": "Sialkot",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "35",
            "created_at": "2018-02-22 04:05:56"
        },
        {
            "issue_id": "235",
            "issue_name": "Gender gap:",
            "issue_category": "Education",
            "text": "Major factors that hinder enrolment rates of girls include poverty, cultural constraints, illiteracy of parents and parental concerns about safety and mobility of their daughters. Society’s emphasis on girl’s modesty, protection and early marriages may limit family’s willingness to send them to school. Enrolment of rural girls is 45% lower than that of urban girls; while for boys the difference is 10% only, showing that gender gap is an important factor.",
            "status": "1",
            "location": "Kasur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "35",
            "created_at": "2018-02-22 08:51:40"
        },
        {
            "issue_id": "237",
            "issue_name": "War on Terror:",
            "issue_category": "Gender",
            "text": "Pakistan’s engagement in war against terrorism also affected the promotion of literacy campaign. The militants targeted schools and students; several educational institutions were blown up, teachers and students were killed in Balochistan, KPK and FATA. This may have to contribute not as much as other factors, but this remains an important factor.",
            "status": "1",
            "location": "Burewala",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "38",
            "created_at": "2018-02-23 04:07:32"
        },
        {
            "issue_id": "238",
            "issue_name": "Funds for Education:",
            "issue_category": "Policy",
            "text": "Pakistan spends 2.4% GDP on education. At national level, 89% education expenditure comprises of current expenses such as teachers’ salaries, while only 11% comprises of development expenditure which is not sufficient to raise quality of education.",
            "status": "1",
            "location": "Khanewal",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-02-24 12:27:59"
        },
        {
            "issue_id": "239",
            "issue_name": "Technical Education:",
            "issue_category": "Gender",
            "text": "Sufficient attention has not been paid to the technical and vocational education in Pakistan. The number of technical and vocational training institutes is not sufficient and many are deprived of infrastructure, teachers and tools for training. The population of a state is one of the main elements of its national power. It can become an asset once it is skilled. Unskilled population means more jobless people in the country, which affects the national development negatively. Therefore, technical education needs priority handling by the government.\n\nPoverty, law and order situation, natural disasters, budgetary constraints, lack of access, poor quality, equity, and governance have also contributed in less enrolments.",
            "status": "1",
            "location": "Khanewal",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "36",
            "created_at": "2018-02-26 11:19:53"
        },
        {
            "issue_id": "240",
            "issue_name": "Bad Condition of Government School in Karachi",
            "issue_category": "Infrastructure",
            "text": "Government ne kaha tha k sab theek hoga aur 2.5 arab lagaenge school ki halat theek karne k liye lekin ab tk koi update ni hai. School istemal ki halat me ni hai",
            "status": "1",
            "location": "Government Boys Primary School  -no.1 gulbai",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "1",
            "created_at": "2018-02-26 11:52:52"
        },
        {
            "issue_id": "244",
            "issue_name": "\nProblems in Education of Pakistan",
            "issue_category": "Gender",
            "text": "Education is very important for every human being. It makes one able to understand what is happening around us logically and clearly. Only educated person has the ability to take practical decisions and make right moves at the right time. Human existence without education is just like fecund land. Education not only enables individuals to put their potential to best use and do something productive in the upcoming future, but also plays a main role in shaping an individual to be a better, responsible citizen and an active member of the society. An educated person with self-confidence and precise moves knows how to transform the world. Education provides the ladder for achieving success in life and enables us to utilize skills and caliber in a constructive way. Therefore, it’s the prime responsibility of an individual to get educated and live a prosperous life while being a responsible citizen.",
            "status": "1",
            "location": "Abbottabad",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:27:19"
        },
        {
            "issue_id": "241",
            "issue_name": "Issues in Rawalpindi",
            "issue_category": "Education",
            "text": "An analysis of the issues and problems suggest that:\n\nThe official data shows the allocation of funds for educational projects but there is no mechanism which ensures the proper expenditure of those funds on education.\n\nThe existing infrastructure is not being properly utilized in several parts of the country.\nThere are various challenges that include expertise, institutional and capacity issues, forging national cohesion, uniform standards for textbook development, and quality assurance.\nThe faculty hiring process is historically known to be politicized. It is because of this that the quality of teaching suffers and even more so when low investments are made in teachers’ training. As a result teachers are not regular and their time at school is not as productive as it would be with a well-trained teacher.\nInside schools there are challenges which include shortage of teachers, teacher absenteeism, missing basic facilities and lack of friendly environment.\nOut of school challenges include shortage of schools, distance – especially for females, insecurity, poverty, cultural norms, parents are reluctant or parents lack awareness.",
            "status": "1",
            "location": "South Karachi",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "36",
            "created_at": "2018-02-26 13:08:59"
        },
        {
            "issue_id": "242",
            "issue_name": "Solution for issues in education system",
            "issue_category": "Infrastructure",
            "text": "There is a need for implementation of national education policy and vision 2030 education goals. An analysis of education policy suggests that at the policy level there are several admirable ideas, but practically there are some shortcomings also.\n\nIt may not be possible for the government at the moment to implement uniform education system in the country, but a uniform curriculum can be introduced in educational institutes of the country. This will provide equal opportunity to the students of rural areas to compete with students of urban areas in the job market.\n\nSince majority of Pakistani population resides in rural areas and the access to education is a major problem for them, it seems feasible that a balanced approach for formal and informal education be adopted. Government as well as non-government sector should work together to promote education in rural areas.",
            "status": "1",
            "location": "Khanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "36",
            "created_at": "2018-02-26 13:09:30"
        },
        {
            "issue_id": "245",
            "issue_name": "Problems in Education",
            "issue_category": "Gender",
            "text": "Following are the problems in education in Pakistan.\n\n         1.    Education System is based on \n             Unequal Lines\nThe educational system of Pakistan is based on unequal lines. Medium of education is different in both, public and private sector. This creates a sort of disparity among people, dividing them into two segments.\n          2.    Regional Disparity\nRegional disparity is also a major cause. The schools in Baluchistan (The Largest Province Of Pakistan By Area) are not that much groomed as that of Punjab (The Largest Province Of Pakistan By Population). In FATA, the literacy rate is deplorable constituting 29.5% in males and 3% in females.\n          3.     Ratio of Gender Discrimination\nThe ratio of gender discrimination is a cause which is projecting the primary school ratio of boys & girls which is 10:4 respectively. For the last few years there has been an increase in the growth of private schools. That not only harms the quality of education but creates a gap among haves and has not.\n4.    Lack of Technical Education\nThe lack of technical education is a biggest flaw in the educational policy that has never been focused before. Therefore, less technical people mean less.\n          5.    Funds\nThe allocation of funds for education is very low. It is only 1.5 to 2.0 percent of the total GDP. It should be around 7% of the total GDP.\n          6.    Untrained Teachers\nThe teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation.\n          7.    Poverty\nPoverty is also another factor that restricts the parents to send their children to public or private schools. So, they prefer to send their children to madrassas where education is totally free.",
            "status": "1",
            "location": "Jaranwala",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:30:08"
        },
        {
            "issue_id": "246",
            "issue_name": "Solutions for Educational System",
            "issue_category": "Policy",
            "text": "Estimating the value of education, the Government should take solid steps on this issue. Implementation instead of projecting policies should be focused on. Allocation of funds should be made easy from provinces to districts and then to educational institutes. Workshops must be arranged for teachers. Foreign states are using LSS system. This should be inducted in Pakistani schools to improve the hidden qualities of children. Technical education must be given to all the classes. The education board of Punjab has projected a plan to give tech- education to the children of industrial workers. Promotion of the primary education is the need of time. Teachers, professors and educationists should be consulted while devising any plan, syllabus or policy. The state seems to give up her responsibility and totally relying on private sector. The need of time is to bring education in its original form to masses. Burdening students with so much books will not work as he will not understand what the world is going to do next moment. Education is the only cure of the instability in the state and can bring revolution through evolution, by eradicating the social evils. This is how to remove illiteracy in Pakistan.",
            "status": "1",
            "location": "Chishtian",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:34:50"
        },
        {
            "issue_id": "247",
            "issue_name": "\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\n",
            "issue_category": "Infrastructure",
            "text": "\nFollowing are the problems in education in Pakistan.\n\n         1.    Education System is based on \n             Unequal Lines\nThe educational system of Pakistan is based on unequal lines. Medium of education is different in both, public and private sector. This creates a sort of disparity among people, dividing them into two segments.\n          2.    Regional Disparity\nRegional disparity is also a major cause. The schools in Baluchistan (The Largest Province Of Pakistan By Area) are not that much groomed as that of Punjab (The Largest Province Of Pakistan By Population). In FATA, the literacy rate is deplorable constituting 29.5% in males and 3% in females.\n          3.     Ratio of Gender Discrimination\nThe ratio of gender discrimination is a cause which is projecting the primary school ratio of boys & girls which is 10:4 respectively. For the last few years there has been an increase in the growth of private schools. That not only harms the quality of education but creates a gap among haves and has not.\n4.    Lack of Technical Education\nThe lack of technical education is a biggest flaw in the educational policy that has never been focused before. Therefore, less technical people mean less.\n          5.    Funds\nThe allocation of funds for education is very low. It is only 1.5 to 2.0 percent of the total GDP. It should be around 7% of the total GDP.\n          6.    Untrained Teachers\nThe teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation.\n          7.    Poverty\nPoverty is also another factor that restricts the parents to send their children to public or private schools. So, they prefer to send their children to madrassas where education is totally free.\n\nRecently, minister of education announced a new Education policy for that next 10 years. The interesting thing is that the previous educational policy from 1998 to 2010 is still not expired. Although it is projected to give new plans and to make more promises with the nation. It is said in this policy that all the public schools will be raised up to the level of private schools. No plan of action have been discussed, yet a notice is issued to private schools to induct government course in 5th and 8th class and these classes will bound to take board exams. This disturbed the students of private sector also.\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\n\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsn 63363$3)?:!:!,!!:’aiaqi",
            "status": "1",
            "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:35:31"
        },
        {
            "issue_id": "248",
            "issue_name": "first issue from app",
            "issue_category": "Gender",
            "text": "Following are the problems in education in Pakistan.\n\n         1.    Education System is based on \n             Unequal Lines\nThe educational system of Pakistan is based on unequal lines. Medium of education is different in both, public and private sector. This creates a sort of disparity among people, dividing them into two segments.\n          2.    Regional Disparity\nRegional disparity is also a major cause. The schools in Baluchistan (The Largest Province Of Pakistan By Area) are not that much groomed as that of Punjab (The Largest Province Of Pakistan By Population). In FATA, the literacy rate is deplorable constituting 29.5% in males and 3% in females.\n          3.     Ratio of Gender Discrimination\nThe ratio of gender discrimination is a cause which is projecting the primary school ratio of boys & girls which is 10:4 respectively. For the last few years there has been an increase in the growth of private schools. That not only harms the quality of education but creates a gap among haves and has not.\n4.    Lack of Technical Education\nThe lack of technical education is a biggest flaw in the educational policy that has never been focused before. Therefore, less technical people mean less.\n          5.    Funds\nThe allocation of funds for education is very low. It is only 1.5 to 2.0 percent of the total GDP. It should be around 7% of the total GDP.\n          6.    Untrained Teachers\nThe teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation.\n          7.    Poverty\nPoverty is also another factor that restricts the parents to send their children to public or private schools. So, they prefer to send their children to madrassas where education is totally free.\n\nRecently, minister of education announced a new Education policy for that next 10 years. The interesting thing is that the previous educational policy from 1998 to 2010 is still not expired. Although it is projected to give new plans and to make more promises with the nation. It is said in this policy that all the public schools will be raised up to the level of private schools. No plan of action have been discussed, yet a notice is issued to private schools to induct government course in 5th and 8th class and these classes will bound to take board exams. This disturbed the students of private sector also.",
            "status": "1",
            "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:37:27"
        },
        {
            "issue_id": "252",
            "issue_name": "first issue from app",
            "issue_category": "Infrastructure",
            "text": "Following are the problems in education in Pakistan.\n\n         1.    Education System is based on \n             Unequal Lines\nThe educational system of Pakistan is based on unequal lines. Medium of education is different in both, public and private sector. This creates a sort of disparity among people, dividing them into two segments.\n          2.    Regional Disparity\nRegional disparity is also a major cause. The schools in Baluchistan (The Largest Province Of Pakistan By Area) are not that much groomed as that of Punjab (The Largest Province Of Pakistan By Population). In FATA, the literacy rate is deplorable constituting 29.5% in males and 3% in females.\n          3.     Ratio of Gender Discrimination\nThe ratio of gender discrimination is a cause which is projecting the primary school ratio of boys & girls which is 10:4 respectively. For the last few years there has been an increase in the growth of private schools. That not only harms the quality of education but creates a gap among haves and has not.\n4.    Lack of Technical Education\nThe lack of technical education is a biggest flaw in the educational policy that has never been focused before. Therefore, less technical people mean less.\n          5.    Funds\nThe allocation of funds for education is very low. It is only 1.5 to 2.0 percent of the total GDP. It should be around 7% of the total GDP.\n          6.    Untrained Teachers\nThe teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation.\n          7.    Poverty\nPoverty is also another factor that restricts the parents to send their children to public or private schools. So, they prefer to send their children to madrassas where education is totally free.\n\nRecently, minister of education announced a new Education policy for that next 10 years. The interesting thing is that the previous educational policy from 1998 to 2010 is still not expired. Although it is projected to give new plans and to make more promises with the nation. It is said in this policy that all the public schools will be raised up to the level of private schools. No plan of action have been discussed, yet a notice is issued to private schools to induct government course in 5th and 8th class and these classes will bound to take board exams. This disturbed the students of private sector also.",
            "status": "1",
            "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:43:35"
        },
        {
            "issue_id": "253",
            "issue_name": "this is first issue from app",
            "issue_category": "Gender",
            "text": "Following are the problems in education in Pakistan.\n\n         1.    Education System is based on \n             Unequal Lines\nThe educational system of Pakistan is based on unequal lines. Medium of education is different in both, public and private sector. This creates a sort of disparity among people, dividing them into two segments.\n          2.    Regional Disparity\nRegional disparity is also a major cause. The schools in Baluchistan (The Largest Province Of Pakistan By Area) are not that much groomed as that of Punjab (The Largest Province Of Pakistan By Population). In FATA, the literacy rate is deplorable constituting 29.5% in males and 3% in females.\n          3.     Ratio of Gender Discrimination\nThe ratio of gender discrimination is a cause which is projecting the primary school ratio of boys & girls which is 10:4 respectively. For the last few years there has been an increase in the growth of private schools. That not only harms the quality of education but creates a gap among haves and has not.\n4.    Lack of Technical Education\nThe lack of technical education is a biggest flaw in the educational policy that has never been focused before. Therefore, less technical people mean less.\n          5.    Funds\nThe allocation of funds for education is very low. It is only 1.5 to 2.0 percent of the total GDP. It should be around 7% of the total GDP.\n          6.    Untrained Teachers\nThe teachers in government schools are not well trained. People who do not get job in any other sector, they try their luck in educational system. They are not professionally trained teachers so they are unable to train a nation.\n          7.    Poverty\nPoverty is also another factor that restricts the parents to send their children to public or private schools. So, they prefer to send their children to madrassas where education is totally free.\n\nRecently, minister of education announced a new Education policy for that next 10 years. The interesting thing is that the previous educational policy from 1998 to 2010 is still not expired. Although it is projected to give new plans and to make more promises with the nation. It is said in this policy that all the public schools will be raised up to the level of private schools. No plan of action have been discussed, yet a notice is issued to private schools to induct government course in 5th and 8th class and these classes will bound to take board exams. This disturbed the students of private sector also.",
            "status": "1",
            "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:43:45"
        },
        {
            "issue_id": "254",
            "issue_name": "this is first issue from app",
            "issue_category": "Infrastructure",
            "text": "Estimating the value of education, the Government should take solid steps on this issue. Implementation instead of projecting policies should be focused on. Allocation of funds should be made easy from provinces to districts and then to educational institutes. Workshops must be arranged for teachers. Foreign states are using LSS system. This should be inducted in Pakistani schools to improve the hidden qualities of children. Technical education must be given to all the classes. The education board of Punjab has projected a plan to give tech- education to the children of industrial workers. Promotion of the primary education is the need of time. Teachers, professors and educationists should be consulted while devising any plan, syllabus or policy. The state seems to give up her responsibility and totally relying on private sector. The need of time is to bring education in its original form to masses. Burdening students with so much books will not work as he will not understand what the world is going to do next moment. Education is the only cure of the instability in the state and can bring revolution through evolution, by eradicating the social evils. This is how to remove illiteracy in Pakistan.",
            "status": "1",
            "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
            "views": "0",
            "share": "0",
            "parent_id": "0",
            "user_id": "40",
            "created_at": "2018-03-05 11:51:19"
        }
    ]
}
							 
							 </pre>
                        </div>
						<div id="popular" class="cont">
                            <h3>Get Popular issues</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_popular_issues</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Record",
    "data": [
        {
            "issue_id": "2",
            "issue_name": "Education: the major problem in Pakistan",
            "status": "1",
            "location": "Lahore Garrison University, Lahore",
            "views": "23",
            "share": "17",
            "isCitizen": "1",
            "issues_media": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1521456216.jpg",
            "created_at": "2018-01-30 11:18:06"
        }
}			
			
							</pre>

                        </div>							
						
						<div id="delete_info" class="cont">
                            <h3>Issue Detail</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/issue_detail</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								
								
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Detail",
    "data": {
        "issue_id": "1",
        "issue_name": "Teachers of Peshawar girls’ school concerned at lack of security",
        "issue_category": "Education",
        "text": "The parents and teachers of the Government Girls Primary School Wazirbagh-I have expressed concern over the lack of proper security arrangement at the school. \nThere were two watchmen. One of which, Kamran Shahzad, had been transferred to the Government Girls Primary School Wazirbagh No-II. The strength of students at that school was far less than Wazirbagh No-I. The other watchman, Muhammad Wali, had some health problem and was facing difficulties in performing his duties. The watchman Muhammad Wali told this scribe that the school was situated in a sensitive area where the police had conducted raids in the past to arrest militants. He said that he had not been provided gun and his job became difficult so he left the job.\nGovernment of KP should take immediate notice and provide the school with armed guards for the safety of children.",
        "status": "1",
        "location": "Government Girls Primary School Wazirbagh No-I",
        "views": "7",
        "share": "12",
        "parent_id": "0",
        "user_id": "1",
        "isAnonymous": "0",
        "created_at": "2018-01-30 11:18:06",
        "issues_media": [
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520421066.png",
                "issues_id": "1",
                "type": "image",
                "caption": "Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520421066.png",
                "issues_id": "1",
                "type": "image",
                "caption": "Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520421066.png",
                "issues_id": "1",
                "type": "image",
                "caption": "Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520421066.png",
                "issues_id": "1",
                "type": "image",
                "caption": "Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
            }
        ],
        "child": [
            {
                "issue_id": "236",
                "issue_name": "Cost of education:",
                "issue_category": "Infrastructure",
                "text": "The economic cost is higher in private schools, but these are located in richer settlements only. The paradox is that private schools are better but not everywhere and government schools ensure equitable access but do not provide quality education.",
                "status": "1",
                "location": "Gujrat",
                "views": "0",
                "share": "0",
                "parent_id": "1",
                "user_id": "35",
                "isAnonymous": "1",
                "created_at": "2018-02-22 12:31:51"
            },
            {
                "issue_id": "261",
                "issue_name": "first issue from app",
                "issue_category": "Infrastructure",
                "text": "Fjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsn 63363$3)?:!:!,!!:’aiaqi",
                "status": "1",
                "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
                "views": "0",
                "share": "0",
                "parent_id": "1",
                "user_id": "40",
                "isAnonymous": "0",
                "created_at": "2018-03-10 12:18:48"
            },
            {
                "issue_id": "262",
                "issue_name": "first issue from app",
                "issue_category": "Infrastructure",
                "text": "Fjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsn 63363$3)?:!:!,!!:’aiaqi",
                "status": "1",
                "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
                "views": "0",
                "share": "0",
                "parent_id": "1",
                "user_id": "40",
                "isAnonymous": "0",
                "created_at": "2018-03-10 12:18:52"
            },
            {
                "issue_id": "263",
                "issue_name": "first issue from app",
                "issue_category": "Gender",
                "text": "Fjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsnHdhjxjfdjx fjfnf fb f dndjdjf f f f. d fd f f f. f ff g G d didkkddjjdekfjjdrnfnjfejnfjffjkefkjfkdnfr\\n\\nFjjfjfkrdjnfjjdfjfnndwkkskkdnfbfjkrwkjdfjnekskdnx jkdneodjdbjc hjkd. Fjdjjndjwhd jdknejdije mnnbd hbbdvvdd njhhdnjhd bbdjdj jnbd. Ffffjnejjddne mjdjdjsn 63363$3)?:!:!,!!:’aiaqi",
                "status": "1",
                "location": "Gps Ghulam Qadar, Basti Karachi, Rajanpur",
                "views": "0",
                "share": "0",
                "parent_id": "1",
                "user_id": "40",
                "isAnonymous": "0",
                "created_at": "2018-03-10 12:18:53"
            }
        ],
        "child_media": [
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520421066.png",
                "issues_id": "236",
                "type": "image",
                "caption": "Sed consequat, leo eget bibendum sodales, augue velit cursus nunc"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520249237.jpg",
                "issues_id": "261",
                "type": "image",
                "caption": "caption1"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520249237.jpg",
                "issues_id": "262",
                "type": "image",
                "caption": "caption1"
            },
            {
                "url": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1520249237.jpg",
                "issues_id": "263",
                "type": "image",
                "caption": "caption1"
            }
        ]
    }
}
							 
							 </pre>

                        </div>
						<div id="timestamp" class="cont">
                            <h3>Create Issue</h3>
                           
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/create_issuse</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_name</td>
								<td>Required</td>
								</tr>
						
								<tr>
								<td>text</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>location</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>isCitizen</td>
								<td>Required</td>
								</tr>
								</table>
								
								
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Added Successfully",
    "points": 0
}

							
							</pre>

                        </div>
						 <div id="uploadmedia" class="cont">
                            <h3> Upload Media</h3>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/upload_media</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>file</td>
								<td>Required</td>
								</tr>
								</table>
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Uploaded Successfully",
    "filename": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1521701661.png",
    "extention": "png"
}
							
							</pre>
                        </div>
						<div id="getcomment" class="cont">
                            <h3>Get Comment</h3>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_comments</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								</table>
                            </div>
                            <pre class="prettyprint">
{
    "error": false,
    "message": "Comments Record",
    "data": [
        {
            "issue_id": "3",
            "user_id": "76",
            "username": null,
            "profile_image": null,
            "text": "gggggh",
            "updated_at": "2018-03-21 18:41:17"
        }
}							
							</pre>
                        </div>
						<div id="addcomment" class="cont">
                            <h3>Add Comments</h3>
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/add_comments</pre>
                                <pre class="method_type">POST</pre>
								<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>text</td>
								<td>Required</td>
								</tr>
								
								</table>
                            </div>
                            <pre class="prettyprint">
{
    "error": true,
    "message": "No Such users found"
}							
							</pre>
                        </div>
						
						<div id="user_info" class="cont">
                            <h3>Update View Count</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/update_views</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>user_id</td>
								<td>Required</td>
								</tr>	
								</table>
							 <pre class="prettyprint">	
{
    "error": false,
    "message": "Issuse Views",
    "data": 8
}							 
							 </pre>

                        </div>
						
						
						<div id="channel_information" class="cont">
                            <h3>Update Share Count</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/update_share</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								
								
								
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Share",
    "data": 13
}
							 
							 </pre>

                        </div>
						<div id="updatepoint" class="cont">
                            <h3>Update Points</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/update_points</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								<tr>
								<td>activity</td>
								<td>Required</td>
								</tr>
								
								
								</table>
							 <pre class="prettyprint">
{
    "error": true,
    "message": "No Such users found"
}
							 
							 </pre>

                        </div>
						
						<div id="getpoint" class="cont">
                            <h3>Get Points</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_points</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Points Record",
    "data": [
        {
            "pt_report_like": "50"
        },
        {
            "pt_report_likes": "0"
        },
        {
            "pt_report_likes": "0"
        }
    ],
    "total_point": "50"
}
							 
							 </pre>

                        </div>
						<div id="getreport" class="cont">
                            <h3>Get my Reports</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_reports</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								<tr>
								<td>issue_id</td>
								<td>Required</td>
								</tr>
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Issuse Record",
    "data": [
        {
            "issue_id": "2",
            "issue_name": "Education: the major problem in Pakistan",
            "status": "1",
            "location": "Lahore Garrison University, Lahore",
            "views": "23",
            "share": "17",
            "isCitizen": "1",
            "issues_media": "http://staging.dplit.com:8080/dev/demo/app/assets/upload/1521456216.jpg",
            "created_at": "2018-01-30 11:18:06"
        }
}						 
							 </pre>

                        </div>
						<div id="leaderboard" class="cont">
                            <h3>Leaderboard</h3>
                            
                            <div class="highlight">
                                <pre class="url_title">URL</pre>
                                <pre class="url"><?php echo base_url();?>V2/get_leaderboard</pre>
                                <pre class="method_type">POST</pre>
                            </div>
							<table>
								<tr>
								<td><b>Parameter</b></td>
								<td><b>Description</b></td>
								</tr>
								</table>
							 <pre class="prettyprint">
{
    "error": false,
    "message": "Points Record",
    "data": [
        {
            "user_id": "39",
            "username": null,
            "points": "6650"
        },
        {
            "user_id": "1",
            "username": "testuser",
            "points": "50"
        },
        {
            "user_id": "45",
            "username": "DPL Testing",
            "points": "50"
        }
    ]
}					 
							 </pre>

                        </div>
                       
					   
					   
                    </div>
                </div>
            </section>

            <footer>
                <div class="">
                    <p> &copy; Copyright Ezespn. All Rights Reserved.</p>
                </div>
            </footer>
        </div>

       
		 <script src="<?php echo base_url();?>docs/js/jquery.min.js"></script>
		 <script src="<?php echo base_url();?>docs/js/prettify/prettify.js"></script>
		<link rel="stylesheet" href=""/>
        <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js?lang=css&skin=sunburst"></script>
		<script src="<?php echo base_url();?>docs/js/layout.js"></script>
       

        <script>
$(function () {
    $('a[href*="#"]:not([href="#"])').click(function () {
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html, body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
    });
});
        </script>
    </body>
</html>
