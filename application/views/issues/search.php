<?php require_once('mainheader.php');?>
<section class="pageTitle">
      <div class="container">
        <div class="title">
          CREATE AN ISSUE
        </div>
      </div>
    </section><!-- pageTitle -->

    <section class="contentContainer">
      <div class="container">

          <div class="searchWrapp">
		  <form method="post" action="<?= base_url('issues/create')?>" id="location_form">
            <p>SEARCH FOR A REGION OR A SCHOOL AND CREATE AN ISSUE</p>
             <input id="data-holder" name="short_name" type="hidden">
            <div class="autoCompleteWrapp homeAutoCompleteWrapp">
              <input type="text" class="searchField" id="basics" name="full_name">
			  <i class="fa fa-search"></i>
			  <!--<button>SEARCH</button>-->
            </div><!-- autoCompleteWrapp -->
			
             </form>
            <!-- <p class="uploadLabel">UPLOAD A SPREADSHEET FOR BATCH PUBLISHING</p>


           <div class="COD_browser-btn browser-btn uploadBtn">
              <span>UPLOAD SPREADSHEET</span>
              <input type="file" id="add-document-file" name="add-document-file" class="file-upload-input COD_file-upload-input" accepts="image/*" title="Choose a document to upload">
            </div>-->

           <div class="vp-browserbtn-progress COD_vp-browserbtn-progress" style="display:none">
              <!--vp-browser-after--> 
              <div class="vp-browser-after">
                 <div class="vp-progress-main">
                    <div class="progress">
                       <div class="progress-bar COD_progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Complete</span>
                       </div>
                    </div>
                 </div>
              </div>
           </div>
            
          </div>

      </div>
    </section> <!-- contentContainer -->
	<?php require_once('mainfooter.php');?>