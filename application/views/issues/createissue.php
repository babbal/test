<?php require_once('mainheader.php');?>
<section class="pageTitle innerPage">
      <div class="container">
        <div class="title">
          CREATE AN ISSUE
        </div>
        <nav class="innerNav">
          <ul>
            <li><a href="" class="active">Compose</a></li>
            <li><a href="">News</a></li>
          </ul>
        </nav>
      </div>
    </section><!-- pageTitle -->

    <section class="breadcrumbWrapp">
      <div class="container">
        <ul>
          <li><span><?php echo $short_name ;?></span></li>
          <li><span>/</span></li>
          <li class="last"><span><?php echo $full_name ;?></span></li>
          <li class="last"><span><i class="fa fa-search"></i></span></li>
        </ul>

      <div class="autoCompleteWrapp">
	   <form method="post" action="<?= base_url('issues/create')?>" id="location_form">
	      <input id="data-holder" name="short_name" type="hidden">
              <input type="text" class="searchField" id="basics" name="full_name">
          </form>   
            </div><!-- autoCompleteWrapp -->
      </div>
    </section>

    <section class="contentContainer mainbodywrapp">
      <div class="container">
<form method="post"  action="<?= base_url('issues/save')?>" enctype="multipart/form-data" onSubmit="return checkform()">
          <div class="innerFormWrapp">
            <div class="formRow">
              <label for="name">TITLE</label>
              <input type="hidden" id="location" name="location" value="<?php echo $full_name ;?>" class="formfield">
			  <input type="text" id="name" name="title" class="formfield">
            </div>

            <div class="formRow">
              <label for="title">TYPE</label>
               <select class="example-getting-started" id="cat_name" name="cat_name[]" multiple="multiple">
                <?php foreach($categories as $record){ ?>      
       			  <option value="<?php echo $record->cat_id?>"><?php echo $record->cat_name?></option>
				<?php }?>
              </select>
            </div>

            <div class="formRow">
              <label for="body">BODY</label>
              <textarea id="description" name="description"></textarea>
            </div>

            <div class="formRow imageEmbededView" style="display:none" id="image_div">
              <label for="">IMAGE</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
                  <img id="imagePreview" src="">
               </div>
               <label for="imageCaption">IMAGE CAPTION</label>
                <input type="text" id="imageCaption" name="imagecaption" class="formfield">
              </div>
            </div><!-- imageEmbededView -->

            <div class="formRow videoEmbededView" style="display:none" id="video_div">
              <label for="">Video</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
                  <iframe id="videoPreview" height="300" src="" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
               </div>
               <label for="videoCaption">Video CAPTION</label>
                <input type="text" id="videoCaption" name="videocaption" class="formfield">
              </div>
            </div><!-- videoEmbededView -->

            <div class="formRow graphEmbededView"style="display:none" id="graph_div">
              <label for="">Chart</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
                  <img id="graphPreview" src="<?= base_url('assets/images/graphImage.jpg')?>">
               </div>
               <label for="chartCaption">Chart CAPTION</label>
			   <input type="hidden"  name="chart" id="chart">
                <input type="text" id="chartCaption" name="chartcaption" class="formfield">
              </div>
            </div><!-- graphEmbededView -->

            <div class="formRow addOnWrapp">
                <p>Add a Text, a Graph, an Image or a Video</p>
                <div>
                  <ul>
                    <li class="textAddition"><a href="JavaScript:Void(0);"><img src="<?= base_url('assets/images/text.png')?>"></a></li>
                     <li class="graphSelection"><a href="JavaScript:Void(0);"><img src="<?= base_url('assets/images/graph.png')?>"></a></li>
                      <li class="imageUploader"><a href="JavaScript:Void(0);"><img src="<?= base_url('assets/images/image.png')?>"></a></li>
                       <li class="videoUploader"><a href="JavaScript:Void(0);"><img src="<?= base_url('assets/images/video.png')?>"></a></li>
                  </ul>
                </div>

                <div class="imageAddingWrapp imageVideoaddition" id="image_select">
                  <span class="closeBtn"><i class="fa fa-seatimes-circle"></i></span>
                  <label>Upload an Image</label>
                  <div class="COD_browser-btn browser-btn uploadBtn">
                      <span>BROWSE IMAGE</span>
                      <input type="file" id="add-document-file" name="image" class="file-upload-input COD_file-upload-input" accepts="image/*" title="">
                  </div>
                  <div class="vp-browserbtn-progress COD_vp-browserbtn-progress" style="display:none">
                    <!--vp-browser-after--> 
                    <div class="vp-browser-after">
                       <div class="vp-progress-main">
                          <div class="progress">
                             <div class="progress-bar COD_progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">60% Complete</span>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                </div><!-- imageAddingWrapp -->

                <div class="videoAddingWrapp imageVideoaddition" id="video_select">
                  <span class="closeBtn"><i class="fa fa-seatimes-circle"></i></span>
                  <label>Paste Video URL</label>
				  <input type="hidden"  name="video" id="video" class="embedField">
				  
                  <input type="url"  name="video_url" id="video_url" class="embedField">
                  <a class="embedVideoBtn" id="get_url">EMBED VIDEO</a>

                  <div class="vp-browserbtn-progress COD_vp-browserbtn-progress" style="display:none">
                    <!--vp-browser-after--> 
                    <div class="vp-browser-after">
                       <div class="vp-progress-main">
                          <div class="progress">
                             <div class="progress-bar COD_progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
                                <span class="sr-only">60% Complete</span>
                             </div>
                          </div>
                       </div>
                    </div>
                 </div>
                </div><!-- videoAddingWrapp -->


              </div>

            <div class="formRow publishBtn">
             <button type="submit" class="">PUBLISH</button>
            </div>

          </div>
</form>
      </div>



      <section class="graphSelectionWrapp">
        <span class="closeBtn"><i class="fa fa-seatimes-circle"></i></span>
        <p>Explore Rawalpindi's Education Ranking across different performance scores</p>

        <ul class="nav nav-tabs">
          <li class=""><a data-toggle="tab" href="#home" id="first-tab-trigger" aria-expanded="true">Infrastructure</a></li>
          <li class="active"><a data-toggle="tab" href="#menu1" id="second-tab-trigger" aria-expanded="false">Education</a></li>
          <li class=""><a data-toggle="tab" href="#menu2" aria-expanded="false">Readiness</a></li>
        </ul>

        <div class="tab-content">

            <div id="home" class="tab-pane fade">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="">
                  <h3>Education Score For 2017</h3>
                  <p>The Education Score is a qualitative measure of a school's ability to retain students, of both sexes, and the quality of education provided. The scores are measured on a scale of 100 points.</p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/1.jpg')?>" class="imageMain">
                      <div class="middle">
                        <button class="ebd_graph" data-id="1">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/2.jpg')?>" class="imageMain">
                      <div class="middle">
                      <button class="ebd_graph" data-id="2">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/3.jpg')?>" class="imageMain">
                      <div class="middle">
                       <button class="ebd_graph" data-id="3">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>

               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/4.jpg')?>" class="imageMain">
                      <div class="middle">
                     <button class="ebd_graph" data-id="4">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/5.jpg')?>" class="imageMain">
                      <div class="middle">
                        <button class="ebd_graph" data-id="5">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
               
              
            </div>

            <div id="menu1" class="tab-pane fade active in">

              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="">
                  <h3>Education Score For 2017</h3>
                  <p>The Education Score is a qualitative measure of a school's ability to retain students, of both sexes, and the quality of education provided. The scores are measured on a scale of 100 points.</p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/6.jpg')?>" class="imageMain">
                      <div class="middle">
                    <button class="ebd_graph" data-id="6">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/7.jpg')?>" class="imageMain">
                      <div class="middle">
                       <button class="ebd_graph" data-id="7">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/8.jpg')?>" class="imageMain">
                      <div class="middle">
                       <button class="ebd_graph" data-id="8">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
              
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/9.jpg')?>" class="imageMain">
                      <div class="middle">
                        <button class="ebd_graph" data-id="9">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/10.jpg')?>" class="imageMain">
                      <div class="middle">
                       <button class="ebd_graph" data-id="10">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>

            <div id="menu2" class="tab-pane fade">
               <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="">
                  <h3>Education Score For 2017</h3>
                  <p>The Education Score is a qualitative measure of a school's ability to retain students, of both sexes, and the quality of education provided. The scores are measured on a scale of 100 points.</p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/11.jpg')?>" class="imageMain">
                      <div class="middle">
                       <button class="ebd_graph" data-id="11">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/12.jpg')?>" class="imageMain">
                      <div class="middle">
                        <button class="ebd_graph" data-id="12">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>

              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/13.jpg')?>" class="imageMain">
                      <div class="middle">
                         <button class="ebd_graph" data-id="13">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
              
               <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/14.jpg')?>" class="imageMain">
                      <div class="middle">
                         <button class="ebd_graph" data-id="14">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
              
              <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                <div class="">
                  <div class="chartCont">
                    <img src="<?= base_url('assets/images/15.jpg')?>" class="imageMain">
                      <div class="middle">
                         <button class="ebd_graph" data-id="15">EMBED GRAPH</button>
                      </div>
                  </div>
                </div>
              </div>
            </div>

          </div>

      </section><!-- graphSelectionWrapp -->


    </section> <!-- contentContainer -->
	<?php require_once('mainfooter.php');?>