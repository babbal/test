<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
		<div class="user-panel">
			<div class="pull-left image">
			  <img src="http://rankings.alifailaan.pk/wp-includes/images/aa_logo.png" class="img-circle" alt="User Image">
			</div>
			<div class="pull-left info">
			  <p>AlifAilaan</p>
			  <!--<a href="#"><i class="fa fa-circle text-success"></i> Online</a>-->
			</div>
		</div>
      <!-- search form -->
		<form action="#" method="get" class="sidebar-form">
			<div class="input-group">
				<input type="text" name="q" class="form-control" placeholder="Search...">
				<span class="input-group-btn">
					<button type="submit" name="search" id="search-btn" class="btn btn-flat">
						<i class="fa fa-search"></i>
					</button>
				</span>
			</div>
		</form>
		<!-- /.search form -->
		<!-- sidebar menu: : style can be found in sidebar.less -->
		<ul class="sidebar-menu" data-widget="tree">
			<li class="header">MAIN NAVIGATION</li>
        
			<li class="active treeview menu-open">
			  <a href="<?= base_url('dashboard')?>">
				<i class="fa fa-dashboard"></i> <span>Dashboard</span>
				<span class="pull-right-container">
				</span>
			  </a>
			</li>
        
			
			
			
			
			<li class="treeview">
				<a href="javascript:;">
					<i class="fa fa-user"></i>
					<span>User Managment</span>
					<span class="pull-right-container">
						<i class="fa fa-angle-left pull-right"></i>
					</span>
				</a>
				<ul class="treeview-menu">
					<li><a href="<?= base_url('dashboard/user_group')?>"><i class="fa fa-users"></i>User List</a></li>
					<!--<li><a href="pages/UI/general.html"><i class="fa fa-users"></i>Manage Groups</a></li>-->
				</ul>
			</li>
			
			
			
			
			
			
			
			
		</ul>
		
    </section>
    <!-- /.sidebar -->
</aside>
