    <script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
   <script src="https://www.gstatic.com/charts/loader.js" ></script>
   <script src="<?= base_url('assets/js/vendor/modernizr-2.8.3.min.js')?>"></script>
   <script src="<?= base_url('assets/js/vendor/bootstrap.min.js')?>"></script> 
   <script src="<?= base_url('assets/js/vendor/jquery.easy-autocomplete.js')?>"></script> 
   <script src="<?= base_url('assets/js/vendor/bootstrap-multiselect.js')?>"></script> 
   <script src="<?= base_url('assets/js/bootstrap-select.js')?>"></script>   
   <script src="<?= base_url('assets/js/plugins.js')?>"></script>   
   <script src="<?= base_url('assets/js/main.js')?>"></script>

    <script type="text/javascript">

    $(document).ready(function() {
      // multiselect
        $('.example-getting-started').multiselect();

        // Image Uploader Show Hide
        $(".imageUploader").click(function(){
            $(".imageAddingWrapp").fadeIn();
        });
        $(".closeBtn").click(function(){
            $(".imageAddingWrapp").fadeOut();
        });

        // Video Uploader Show Hide
        $(".videoUploader").click(function(){
            $(".videoAddingWrapp").fadeIn();
        });
        $(".closeBtn").click(function(){
            $(".videoAddingWrapp").fadeOut();
        });

      // Graphs Uploader Show Hide
        $(".graphSelection").click(function(){
            $(".graphSelectionWrapp").fadeIn();
            $(".innerFormWrapp").fadeOut();
        });
        $(".graphSelectionWrapp .closeBtn").click(function(){
            $(".innerFormWrapp").fadeIn();
            $(".graphSelectionWrapp").fadeOut();
        });
        $(".chartCont button").click(function(){
            $(".innerFormWrapp").fadeIn();
            $(".graphSelectionWrapp").fadeOut();
        });

        // Graphs Uploader Show Hide
        $(".breadcrumbWrapp ul .fa-search").click(function(){
            $(".breadcrumbWrapp .autoCompleteWrapp").fadeIn();
        });


    });

	 var options = {

	  url: "<?= base_url('assets/json.json')?>",

	  getValue: "full_name",

	  list: {
	// onSelectItemEvent: function() {
			// var value = $("#basics").getSelectedItemData().name;
            // var a=$("#data-holder").val(value).trigger("change");
			
			//$('#babbal').submit();
		// },
		onClickEvent: function() {
			var value = $("#basics").getSelectedItemData().province;
            var a=$("#data-holder").val(value).trigger("change");
			$('#location_form').submit();
		},
	maxNumberOfElements: 10,		
		match: {
		  enabled: true
		}
	  },

	  theme: "square"
	};
		// $("#basics").keyup(function() {
		// var a=$(this).val().length;

		// if(a > 3){
		// $("#basics").easyAutocomplete(options);
		// }else{
		// return false;	
		// }
		 // });

     $("#basics").easyAutocomplete(options);

    //Add Document File Picker 
    $(".COD_browser-btn").on("change", function(e) {      
        $(this).show();
        $('.COD_vp-browserbtn-progress').show();
        $('.vp-progress-main').show();
        setTimeout(function() {
            $('.COD_progress-bar').css('width', '100%');
            $('.COD_progress-bar').css('transition', 'width 6s ease');
        }, 200); // WAIT 4 second
        setTimeout(function() {
            $('.COD_vp-browserbtn-progress').hide();
            $('.COD_progress-bar').hide();
            $('.vp-progress-main').hide();
			$('#image_select').fadeOut();
           // $('.COD_file-upload-input').val("");
            $('.COD_progress-bar').removeAttr('style');
            $('.COD_vp-actions').show();

        },6000); // WAIT 4 second
    });

    //Add Document File Picker 
    $(".embedVideoBtn").on("click", function(e) {
		var url=$("#video_url").val();
		if(url){
			$("#video").val(url);
			$(this).show();
        $('.COD_vp-browserbtn-progress').show();
        $('.vp-progress-main').show();
        setTimeout(function() {
            $('.COD_progress-bar').css('width', '100%');
            $('.COD_progress-bar').css('transition', 'width 6s ease');
        }, 200); // WAIT 4 second
        setTimeout(function() {
            $('.COD_vp-browserbtn-progress').hide();
            $('.COD_progress-bar').hide();
            $('.vp-progress-main').hide();
			$('#video_select').fadeOut();
           // $('.COD_file-upload-input').val("");
            $('.COD_progress-bar').removeAttr('style');
            // $('.info-display-table-document').append(appendrow);
            $('.COD_vp-actions').show();
			$('#video_url').val("");
             $('#videoPreview').attr('src', url);
			  $("#video_div").fadeIn();	
        }, 6000); // WAIT 4 second
		      
		}else{
			            $('#errors').show();
						$('#errors').html("Please Enter Url");
						$("html, body").animate({
							scrollTop: 0
						}, 200); 
						setTimeout(function() {
							$('#errors').fadeOut('fast');
					  }, 5000); 
						 return false;
		}
        
    });
	function readIMG(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
				setTimeout(function() {
                $('#image_div').fadeIn();
                $('#imagePreview').attr('src', e.target.result);
        }, 6000);
				
				
				
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
	$("#add-document-file").on("change", function()
    {
		   readIMG(this);
		
    });
	
	$(".ebd_graph").on("click", function()
    {
		  var graph=$(this).attr("data-id")
		 if(graph){
		   var url='<?php echo base_url();?>assets/images/'+graph+".jpg";
		   $('#chart').val(url);
		   $('#graphPreview').attr('src', url);
		   $('#graph_div').fadeIn();
		 }
    });
	// $("#get_url").on("click", function()
    // {
		  // var url=$("#video").val();
		 
		  // if(url){
			  // $('#videoPreview').attr('src', url);
			  // $("#video_div").fadeIn();
			  
		  // }else{
			  // alert("Please Enter Url");
		  // }
		
    // });
		function checkform(){
			var name=$('#name').val();
			var cat_name=$('#cat_name').val();
			var description=$('#description').val();
			
			if(name == ""){
					
					$('#errors').show();
					$('#errors').html("Please Enter Type");
					$("html, body").animate({
							scrollTop: 0
						}, 200); 
					setTimeout(function() {
				   
				   $('#errors').fadeOut('fast');
				  }, 5000); 
				return false;
			
			}
			if(cat_name == null){
				
					$('#errors').show();
					$('#errors').html("Please Select Categories");
					$("html, body").animate({
							scrollTop: 0
						}, 200); 
					setTimeout(function() {
						$('#errors').fadeOut('fast');
				  }, 5000); 
					 return false;
			}
			if(description == ""){
				        $('#errors').show();
						$('#errors').html("Please Enter Description");
						$("html, body").animate({
							scrollTop: 0
						}, 200); 
						setTimeout(function() {
							$('#errors').fadeOut('fast');
					  }, 5000); 
						 return false;
			
			}
			
		}
		
		
		
		
		setTimeout(function() {
           $('#error').fadeOut('fast');
          }, 5000); 
	setTimeout(function() {
          $('#success').fadeOut('fast');
           }, 5000); 
	
    </script>
   

    </body>
</html>

