<?php require_once('mainheader.php');
 
 

 
 $this->db->select('district');
 $this->db->group_by('district');
 $this->db->from('issues');
 $this->db->where('district !=','');
 $district = $this->db->get()->result();
 
 
 // echo"<pre>";
 // print_r($district);
 // exit;
  // $this->db->select('*');
 // $this->db->from('issues');
 // $this->db->group_by('issues.district');
 // $this->db->group_by('issues.user_id');  
 // $this->db->join('wp_volunteers', 'issues.user_id = wp_volunteers.volunteer_id');
 // $this->db->where('issues.district IS NOT NULL', null, false);
 // $district = $this->db->get()->result();
?>
<style>
.bootstrap-select:not([class*="col-"]):not([class*="form-control"]):not(.input-group-btn) {
    width: 100%;
}
.form-group{
	position:relative;
}
.searchList{
	padding:0;
	height: 150px;
    position: absolute;
    width: 100%;
    background: white;
    z-index: 2;
    border: 1px solid #cccccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
    overflow-x: auto;
}
li.user_record {
    padding: 5px 10px;
    border-bottom: 1px solid #ccc;
	cursor:pointer;
}

li.user_record:hover {
    background: #ccc;
}
</style>
    <section class="contentContainer">
      <div class="container">

          <div class="indexMenuWrapp">
            <p>What would you like to do today?</p>
            <ul>
              <!--<li><a href="<?php //echo base_url('issues/search')?>">CREATE AN ISSUE</a></li>-->
              <li><a href="<?php echo  base_url('issues/get_issuses')?>">ISSUES</a></li>
			   <li><a href="<?php echo  base_url('issues/csv')?>">DOWNLOAD CSV</a></li>
			   <li><a href="javascript:;" data-toggle="modal" data-target="#myModal">SEND NOTIFICATION</a></li>
			   <!-- <li><a href="<?php// echo  base_url('issues/issuse_flag')?>">BROWSE ISSUES FLAG</a></li>
			   
              <li><a href="<?php //echo  base_url('issues/get_flag')?>">BROWSE COMMENTS FLAG</a></li>-->
            </ul>
          </div>
		  
		  <div class="issueCount">
			
			<div class="individualIssues totalissues">
				<h3>Total Issues</h3>
				<span><?php echo $total; ?></span>
				
			</div>
			<div class="individualIssues approvedissues">
				<h3>Approved Issues</h3>
				<span><?php echo $approve; ?></span>
			</div>
			<div class="individualIssues rejectedissues">
				<h3>Rejected Issues</h3>
				<span><?php echo $rejected; ?></span>
			</div>
			<div class="individualIssues flaggedissues">
				<h3>Flagged Issues</h3>
				<span><?php echo $flag ?></span>
			</div>
			<div class="individualIssues awaitingapproval">
				<h3>Awaiting Approval</h3>
				<span><?php echo $unapprove ?></span>
			</div>
			
		  </div>

      </div>
    </section> <!-- contentContainer -->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Send Notification</h4>
      </div>
      <div class="modal-body">       
                 <form action="<?php echo  base_url('v2/add_notification')?>" method="post" onsubmit="return validateForm()" enctype="multipart/form-data">
				 <div class="form-group">
					<div class="form-group" >
					<input type="radio"  name="method" value="3" checked="checked"> District 
					 <input type="radio"  name="method" value="2"> All User
				  <input type="radio"  name="method" value="1"> Single User
				 
				  
				 </div></div>
				 <div class="form-group" id="all" style="display:none">
				 <div class="form-group" >
				 <input type="hidden" class="head-search" placeholder="SEARCH" id="user_id" name="user_id" />
				 <input type="text" class="form-control" placeholder="SEARCH" id="record" name="record" autocomplete="off"/>
                     <ul id="fetched" style="display:none;" class="form-control searchList">
					 </ul>

					<!--<div class="form-group" >
						   <select class="selectpicker" data-live-search="true" title="User List" id="user_id" name="user_id">
							 <?php 
							// foreach($users as $users){
									// ?> 
							<option value="<?php echo $users->volunteer_id; ?>"><?php echo $users->username; ?></option>		  
							 <?php //}?>  
						   </select>
						</div>-->
						</div>
				</div>
				
				<div class="form-group" id="district">
					<div class="form-group" >
						   <select class="selectpicker" data-live-search="true" title="District List" name="district">
							 <?php 
							 foreach($district as $district){
									 ?> 
							<option value="<?php echo $district->district; ?>"><?php echo $district->district; ?></option>		  
							 <?php }?>  
						   </select>
						</div>
				</div>
				
				<div class="form-group">
				<label for="email" style="color:red">Title (Max 25 characters)</label>
				<input type="text" class="form-control" id="title" maxlength="25" name="title" placeholder="Notification Tilte">
				<span id="title_error" style="display:none;color:red">Title field required</span>
				</div>
				
				<div class="form-group">
				<label for="email" style="color:red">Message Body (Max 200 characters)</label><br>
				<textarea class="form-control" cols="" id="message" rows="10" maxlength="200" type="text" name="message" style="width:100%;"></textarea>
				<span id="message_error" style="display:none;color:red">Message field required</span>
				</div>
			
      </div>
      <div class="modal-footer">
         <input type="submit" class="btn btn-success btn-sm" name="submit"  value="Send" />
	
        </div>
			</form>
    </div>

  </div>
</div>
  <?php require_once('mainfooter.php');?>
  <script>
  $(document).ready(function() {
    $("input[name$='method']").click(function() {
        var user = $(this).val();
		if(user==1){
			$("#all").show();
			$("#district").hide();
		}
		if(user==2){
			$("#all").hide();
			$("#district").hide();
		}
		if(user==3){
			$("#all").hide();
			$("#district").show();
		}
		
//$('.selectpicker').selectpicker('refresh');
    });
});
// setTimeout(function(){ 
// $('.bs-searchbox input').on('click', function (e) {
              // alert();
// }); 
 // }, 6000);


$("#record").keyup(function() {
var dInput = $(this).val();
var n = dInput.length;
var url = '<?php echo base_url()?>v2/getfield';
if(n > 3){
	 document.getElementById('fetched').style.display = "block";
 $.ajax({
                    'url' : url,
                    'type' : 'POST',
                    'data'    : {
                          record : dInput
                         },

                    success : function(data){
						
                    document.getElementById('fetched').innerHTML = data;     
                    }
                });  
     
        }else{
	 
	       document.getElementById('fetched').style.display = "none";
        }
 
 });



$('body').on("click", ".user_record", function(){
   	var id=$(this).data("id");
	var name=$(this).data("name");
	$("#record").val(name);
	$("#user_id").val(id);
	document.getElementById('fetched').style.display = "none";
   	});	
	
$(window).click(function() {
	
	document.getElementById('fetched').style.display = "none";
});

function validateForm() {
    
        var title=$("#title").val();
	    var message=$("#message").val();
	    if(title=="")
		{
			$("#title_error").show();
			 return false;
		}
		if(message=="")
		{
			
			$("#message_error").show();
			 return false;
		}
       
    
}
$("#title").keyup(function() {
$("#title_error").hide();
 
 });
 $("#message").keyup(function() {
$("#message_error").hide();
 
 });
  </script>