 <?php require_once('mainheader.php');?>
 <!-- <section class="pageTitle">
  <div class="container">
    <div class="title">
      APPROVE CITIZEN REPORTS
    </div>
  </div>
</section>pageTitle -->

<?php  
if($issuse){
  foreach($issuse as $key){
   $this->db->select('*');
   $this->db->where('issues_id', $key->issue_id);
   $this->db->from('issues_media');
   $media = $this->db->get()->result();

   $this->db->select('*');
   $this->db->where('volunteer_id', $key->user_id);
   $this->db->from('wp_volunteers');
   $user = $this->db->get()->result();

   $cat=explode(",",$key->issue_category);
   $timestamp = strtotime($key->created_at);
   $this->db->select('*');
   $this->db->where('issue_id', $key->issue_id);
   $this->db->from('issues_na');
   $na = $this->db->get()->result();
   $na_list=array();
   if($na){
	   foreach($na as $na){
		$na_list[]="NA-".$na->na_id;   
		
	   }
   }
 
   $na_list=implode(",",$na_list);
   ?>
   <section class="contentContainer mainbodywrapp acrWrapp">
    <div class="container">
      <form method="post"  action="<?= base_url('letters/add_letter')?>" name="g_form<?php echo $key->issue_id?>"  id="g_form<?php echo $key->issue_id?>">
        <div class="innerFormWrapp">


          <div class="formRow issueMeta">
            <label for=""><?php echo @$user[0]->username; ?></label>
            <label for=""><?php echo $newDate = date('d F Y', $timestamp); ?></label>
          </div>
          <div class="formRow">
            <label for="name">LOCATION</label>
            <input type="text" id="name" class="formfield"  value="<?php echo $key->location;?>" disabled>
          </div>
          <div class="formRow">
            <label for="name">TITLE</label>
            <input type="text" id="name" class="formfield"  name="issue_name" value="<?php echo $key->issue_name;?>" required>

          </div>

          <div class="formRow">
            <label for="title">TYPE</label>
            <select class="example-getting-started" multiple="multiple" name="cat_name[]">
              <?php foreach($categories as $record){ 

                if(in_array($record->cat_id, $cat))
                {
                  ?>      
                  <option value="<?php echo $record->cat_id?>" selected><?php echo $record->cat_name?></option>
                  <?php }else{?>
                  <option value="<?php echo $record->cat_id?>"><?php echo $record->cat_name?></option>
                  <?php }
                }?>
              </select>
            </div>

            <div class="formRow">
              <label for="body">Text</label>
              <textarea id="body" name="description"><?php echo $key->text;?></textarea>
            </div>
            <?php if($media){
              foreach($media as $row){
               if($row->type=="image"){		
                ?>
                <div class="formRow imageEmbededView">
                  <label for="">IMAGE</label>
                  <div class="imageEmbedWrapp">
                   <div class="embedContainer">
                    <img src="<?php echo @$row->link?>">
                  </div>
                  <label for="imageCaption">IMAGE CAPTION</label>
                  <input type="text" id="imageCaption" name="imagecaption[]" class="formfield" value="<?php echo @$row->caption?>">
                  <input type="hidden" id="imageids" name="imageids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>">
                  <input type="hidden" id="image_links" name="image_links[]" class="formfield" value="<?php echo @$row->link?>">
                </div>
              </div><!-- imageEmbededView -->
              <?php }if($row->type=="video"){?>

              <div class="formRow videoEmbededView">
                <label for="">Video</label>
                <div class="imageEmbedWrapp">
                 <div class="embedContainer">
                   <video width="400" controls preload="none" controlsList="nodownload">
                     <source src="<?php echo @$row->link?>" type="video/mp4">
                       <source src="<?php echo @$row->link?>" type="video/ogg">
                         Your browser does not support HTML5 video.
                       </video>
                     </div>
                     <label for="videoCaption">Video CAPTION</label>
                     <input type="text" id="videoCaption" name="videocaption[]" class="formfield" value="<?php echo @$row->caption?>">
                     <input type="hidden" id="videoids" name="videoids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>">
                     <input type="hidden" id="videourl" name="videourl[]" class="formfield" value="<?php echo @$row->link?>">
                   </div>
                 </div><!-- videoEmbededView -->

                 <!--<div class="formRow">
                  <label for="name">Template on Video</label><br>
                  <input type="checkbox" name="template" value="1"> Want Template on Video<br>

                </div>-->
                <?php }if($row->type=="chart"){?>

                <div class="formRow graphEmbededView">
                  <label for="">Chart</label>
                  <div class="imageEmbedWrapp">
                   <div class="embedContainer">
                    <img src="<?php echo @$row->link?>">
                  </div>
                  <label for="chartCaption">Chart CAPTION</label>
                  <input type="text" id="chartCaption" name="chartcaption[]" class="formfield" value="<?php echo @$row->caption?>">
                  <input type="hidden" id="chartids" name="chartids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>">
                </div>
              </div><!-- graphEmbededView -->

              <?php }
            }
          }
          ?>
          <input type="hidden" value="<?php echo $key->issue_id?>" name="issue_id">
	  <input type="hidden" value="<?php echo $na_list?>" name="na_list">
          <input type="hidden" value="<?php echo $key->user_id?>" name="user_id">
	  <input type="hidden" value="<?php echo $newDate?>" name="newDate">
          <input type="hidden" value="<?php echo $this->uri->segment(3);?>" name="page_no">
          <input type="hidden" value="<?php echo @$user[0]->username?>" name="guest-author">
          <input type="hidden" value="<?php echo @$user[0]->email?>" name="guest-email">
          <input type="hidden" value="<?php echo @$user[0]->phone?>" name="guest-phone">
          <input type="hidden" value="<?php echo $key->isCitizen?>" name="guest-isCitizen">
          <input type="hidden" id="title_issuse" class="formfield"  name="title_issuse" value="<?php echo $key->issue_name;?>">

          <div class="formRow publishBtn">

          </div>


         <!--<div class="formRow publishBtn">
        
           <button class="approve">Send as a Letter</button>
         </div>-->
       </form>

     </div>

   </div>

     <div class="ciWrapp">
		<h2>Contact Info</h2>
                 <?php if($user[0]->username){?>
		<span><label>Username:</label><?php echo $user[0]->username; ?></span>
                 <?php }?>
                <?php if($user[0]->email){?>
		<span><label>Email:</label><?php echo $user[0]->email; ?></span>
                 <?php }?>
		 <?php if($user[0]->phone){?>
		<span><label>Phone:</label><?php echo $user[0]->phone; ?></span>
                 <?php }?>
		<!--<span><label>Phone:</label></span>
		<span><label>Faceboook:</label></span>
<?php 

if($key->status==1){?>
	<button type="button" class="btn btn-success">Approved</button>
<?php }elseif($key->status==2){ ?>
<button type="button" class="btn btn-danger">Disapprove</button>
<?php }elseif($key->status==0){ ?>
<button type="button" class="btn btn-info">Unapprove</button>
<?php }else{ ?>
	<button type="button" class="btn btn-danger">Rejected</button>
<?php } ?>

	
	</div>-->
 </section> <!-- contentContainer -->
 <?php }

 echo"<center>";
 echo $links; 
 echo"</center>";

}else{
	
	echo "No Issuse Found";
}?>

<?php require_once('mainfooter.php');?>