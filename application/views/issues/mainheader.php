<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Alifailaan</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
		<?php if($this->uri->segment(2)=="single_issue"){
			
			if(@$issuse[0]->issue_name){
				$tilte=$issuse[0]->issue_name;
				$actual_link=(isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
				
				 $this->db->select('*');
			     $this->db->where('issues_id', $issuse[0]->issue_id);
			     $this->db->from('issues_media');
			     $media = $this->db->get()->result();
				
                    if(@$media[0]->type=="image"){
						$image=@$media[0]->link;
					}elseif(@$media[0]->type=="video"){
						$image=@$media[0]->thumbnail;
					}else{
						$image="https://elections.alifailaan.pk/wp-includes/images/meta-image.png";
					}
				
				
			}else{
				$tilte="Elections 2018 | #TaleemDo";
				$actual_link= str_replace("/app","",base_url()); 
			}
			if(@$issuse[0]->text){
				$text=$issuse[0]->text;
			}else{
				$text="An online platform to curate Pakistan's demand for quality education ahead of this year's general elections.";
			}
			 ?>
		<meta property="og:title" content="<?php echo $tilte;?>" />
		<meta property="og:description" content="<?php echo $text;?>" />
		<meta property="og:image" itemprop="image" content="<?php echo $image;?>" />
		<meta property="og:image:secure_url" content="<?php echo $image;?>" /> 
		<meta property="og:image:type" content="image/jpeg" /> 
		<meta property="og:image:width" content="400" /> 
		<meta property="og:image:height" content="300" />
		<meta property="og:url" content="<?php echo $actual_link?>">
		<meta property="og:type" content="website" />
		<meta property="twitter:image" content="<?php echo $image;?>">
		<?php }?>
		
		

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap.css?<?php echo filemtime('assets/css/bootstrap.css'); ?>">
         <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap-select.css?<?php echo filemtime('assets/css/bootstrap-select.css'); ?>">
        <link rel="stylesheet" href="<?= base_url();?>assets/css/common.css?<?php echo filemtime('assets/css/common.css'); ?>">        
        <link rel="stylesheet" href="<?= base_url();?>assets/css/home.css?<?php echo filemtime('assets/css/home.css'); ?>">   
        <link rel="stylesheet" href="<?= base_url();?>assets/css/bootstrap-multiselect.css?<?php echo filemtime('assets/css/bootstrap-multiselect.css'); ?>"> 
        <link rel="stylesheet" href="<?= base_url();?>assets/css/easy-autocomplete.css?<?php echo filemtime('assets/css/easy-autocomplete.css'); ?>">  		
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
		
		
		

    </head>
    <body>

    <header class="topHeader">

        <div class="container">
          <div class="logo">
              <a href="<?= base_url()."issues"?>" alt="Logo"><img src="<?= base_url('assets/images/logo.png')?>"></a>
          </div><!-- logo -->

          
             <span>
<?php if($this->uri->segment(2)=="get_issuses"){
              $class1="";
              $class2="active"; 
              $class3="";  
              $class4=""; 
              $class5="";  
	      $class6=""; 
              $class7=""; 
 $class8="";  	      
              echo"Browse Issues";
}elseif($this->uri->segment(2)=="issuse_flag"){
              $class1="";
              $class2=""; 
              $class3="active";  
              $class4=""; 
              $class5="";  
	      $class6=""; 
              $class7="";  
	       $class8="";  
              echo"Browse Issues";
}elseif($this->uri->segment(2)=="get_flag"){
              $class1="";
              $class2=""; 
              $class3="";  
              $class4="active"; 
              $class5="";  
	      $class6=""; 
              $class7="";  
	       $class8="";  
              echo"Browse Issues";
 }elseif($this->uri->segment(2)=="all_issues"){
              $class1="";
              $class2=""; 
              $class3="";  
              $class4=""; 
              $class5="active";  
	      $class6=""; 
              $class7="";  
	       $class8="";  
              echo"Browse Issues";
 }elseif($this->uri->segment(2)=="unpublish"){
              $class1="";
              $class2=""; 
              $class3="";  
              $class4=""; 
              $class5="";  
	      $class6="active"; 
              $class7="";  
 $class8="";  	      
              echo"Letters to Politicians";
 }elseif($this->uri->segment(2)=="publish"){
              $class1="";
              $class2=""; 
              $class3="";  
              $class4=""; 
              $class5="";  
	      $class6="";  
	      $class7="active";  
	       $class8="";  
              echo"Letters to Politicians";
 }elseif($this->uri->segment(2)=="approved_issues"){
              $class1="";
              $class2=""; 
              $class3="";  
              $class4=""; 
              $class5="";  
	      $class6="";  
	      $class7=""; 
              $class8="active";  	      
              echo"Browse Issues";
 }else{
              $class1="active";
              $class2=""; 
              $class3="";  
              $class4=""; 
              $class5=""; 
              $class6=""; 
              $class7=""; 
              $class8="";   	      
              echo"ALIF AILAAN - ELECTION APPLICATION";           
}?>
          </span>
          
        </div>

    </header><!-- topHeader -->
	<?php if(($this->uri->segment(2)) && ($this->uri->segment(1)=="issues") && ($this->uri->segment(2)!="single_issue")){?>
	 <nav class="navbar navbar-default">
  <div class="container-fluid">
   <ul class="nav navbar-nav">
      <li class="<?php echo $class1; ?>"><a href="<?php echo base_url()."issues"?>"><img src="<?php echo  base_url('assets/images/left-arrow.svg')?>" width="20px" /></a></li>
      <li class="<?php echo $class2; ?>"><a href="<?php echo base_url()."issues/get_issuses"?>">Awaiting Approval </a></li>
      <li class="<?php echo $class8; ?>"><a href="<?php echo base_url()."issues/approved_issues"?>">Approved Issues</a></li>
      <li class="<?php echo $class5; ?>"><a href="<?php echo base_url()."issues/all_issues"?>">All Issues</a></li>
      <li class="<?php echo $class3; ?>"><a href="<?php echo base_url()."issues/issuse_flag"?>">Flagged Issues</a></li>
      <li class="<?php echo $class4; ?>"><a href="<?php echo base_url()."issues/get_flag"?>">Flagged Comments</a></li>
    </ul>
  </div>
</nav>
<?php }elseif(($this->uri->segment(2)) && ($this->uri->segment(1)=="letters")){?>
	<nav class="navbar navbar-default">
  <div class="container-fluid">
   <ul class="nav navbar-nav">
      <li class="<?php echo $class1; ?>"><a href="<?php echo base_url()."issues"?>"><img src="<?php echo  base_url('assets/images/left-arrow.svg')?>" width="20px" /></a></li>
      <li class="<?php echo $class6; ?>"><a href="<?php echo base_url()."letters/unpublish"?>">UnPublish Letters </a></li>
      <li class="<?php echo $class7; ?>"><a href="<?php echo base_url()."letters/publish"?>">Publish Letters </a></li>
      
    </ul>
  </div>
</nav>
<?php }elseif(($this->uri->segment(2)) && ($this->uri->segment(1)=="issues")&& ($this->uri->segment(2)=="single_issue")){
	
}else{
	
}?>
	<div class="forms-error" style="position: absolute; margin: 1% 35%; width: 30%; z-index: 99;">
		             <div class="alert alert-dismissible alert-danger" id="errors" style="display:none">
					  <button type="button" class="close" data-dismiss="alert">&times;</button>
					  <?php echo "Please Select Atleast Two Skills"; ?>
					</div>
					<?php if($this->session->flashdata('log_error') != "") : ?>
					<div class="alert alert-dismissible alert-danger" id="error">
					  <button type="button" class="close" data-dismiss="alert">&times;</button>
					  <?php echo $this->session->flashdata('log_error'); ?>
					</div>						
				<?php endif; ?>
				<?php if($this->session->flashdata('log_success') != "") : ?>
					<div class="alert alert-dismissible alert-success" id="success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $this->session->flashdata('log_success'); ?>
					</div>					
				<?php endif; ?>
			</div>