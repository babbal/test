 <?php require_once('mainheader.php');?>
 <!--<section class="pageTitle">
      <div class="container">
        <div class="title">
          APPROVE CITIZEN REPORTS
        </div>
      </div>
    </section> pageTitle -->

<?php  
if($issuse){
foreach($issuse as $key){
	        $this->db->select('*');
			$this->db->where('issues_id', $key->issue_id);
			$this->db->from('issues_media');
			$media = $this->db->get()->result();
			
			$this->db->select('*');
			$this->db->where('volunteer_id', $key->user_id);
			$this->db->from('wp_volunteers');
			$user = $this->db->get()->result();
			
			$this->db->select('*');
			$this->db->from('issues_categories');
			$categories = $this->db->get()->result();
			
	         $cat=explode(",",$key->issue_category);
	         $timestamp = strtotime($key->created_at);
			 
			 
			$this->db->select('*');
			$this->db->from('issue_flag_data');
			$this->db->where('issue_flag_id',$key->issue_id);
			$this->db->group_by('user_id');
			$res_flag =$this->db->get()->result();
			$qwer=count($res_flag); 
			
			
	?>
    <section class="contentContainer mainbodywrapp acrWrapp">
      <div class="container">
          <form method="post"  action="<?= base_url('issues/approve_issue_flag')?>" name="g_form<?php echo $key->issue_id?>"  id="g_form<?php echo $key->issue_id?>">
          <div class="innerFormWrapp">

            
            <div class="formRow issueMeta">
              <label for=""><?php echo @$user[0]->username; ?></label>
              <label for=""><?php echo $newDate = date('d F Y', $timestamp); ?></label>
            </div>
			<div class="formRow issueMeta">
              <label for="">Reported By&nbsp;<?php echo $qwer?>
			  <?php if($qwer > 1){
				  echo "Users";
			  }else{
				   echo "User";
			  }?>
			  </label>
              
            </div>
            <div class="formRow">
              <label for="name">LOCATION</label>
              <input type="text" id="name" class="formfield"  value="<?php echo $key->location;?>" disabled>
            </div>
            <div class="formRow">
              <label for="name">TITLE</label>
              <input type="text" id="name" class="formfield"  value="<?php echo $key->issue_name;?>" disabled>
			 
            </div>

            <div class="formRow">
              <label for="title">TYPE</label>
               <select class="example-getting-started" multiple="multiple" name="cat_name[]" disabled>
                  <?php foreach($categories as $record){ 
				  
				  if(in_array($record->cat_id, $cat))
                         {
				  ?>      
       			  <option value="<?php echo $record->cat_id?>" selected><?php echo $record->cat_name?></option>
				<?php }else{?>
					 <option value="<?php echo $record->cat_id?>"><?php echo $record->cat_name?></option>
				<?php }
				  }?>
              </select>
            </div>

            <div class="formRow">
              <label for="body">Text</label>
              <textarea id="body" name="description" disabled><?php echo $key->text;?></textarea>
            </div>
         <?php if($media){
			 foreach($media as $row){
			if($row->type=="image"){		
			 ?>
            <div class="formRow imageEmbededView">
              <label for="">IMAGE</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
                  <img src="<?php echo @$row->link?>">
               </div>
               <label for="imageCaption">IMAGE CAPTION</label>
                <input type="text" id="imageCaption" name="imagecaption[]" class="formfield" value="<?php echo @$row->caption?>" disabled>
				<input type="hidden" id="imageids" name="imageids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>" disabled>
              </div>
            </div><!-- imageEmbededView -->
			<?php }if($row->type=="video"){?>
		   
		   <div class="formRow videoEmbededView">
              <label for="">Video</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
					<video width="400" controls preload="none">
					<source src="<?php echo @$row->link?>" type="video/mp4">
					<source src="<?php echo @$row->link?>" type="video/ogg">
					Your browser does not support HTML5 video.
					</video>
               </div>
               <label for="videoCaption">Video CAPTION</label>
                <input type="text" id="videoCaption" name="videocaption[]" class="formfield" value="<?php echo @$row->caption?>" disabled>
				<input type="hidden" id="videoids" name="videoids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>" disabled>
              </div>
            </div><!-- videoEmbededView -->
            <?php }if($row->type=="chart"){?>
			
            <div class="formRow graphEmbededView">
              <label for="">Chart</label>
              <div class="imageEmbedWrapp">
               <div class="embedContainer">
                  <img src="<?php echo @$row->link?>">
               </div>
               <label for="chartCaption">Chart CAPTION</label>
                <input type="text" id="chartCaption" name="chartcaption[]" class="formfield" value="<?php echo @$row->caption?>" disabled>
              <input type="hidden" id="chartids" name="chartids[]" class="formfield" value="<?php echo @$row->media_issuse_id?>" disabled>
			  </div>
            </div><!-- graphEmbededView -->
			
			<?php }
			}
			}
			?>
			<input type="hidden" value="<?php echo $key->issue_id?>" name="issue_id">
			<input type="hidden" value="<?php echo $key->user_id?>" name="user_id">
			 <input type="hidden" id="title_issuse" class="formfield"  name="title_issuse" value="<?php echo $key->issue_name;?>">
			
            <div class="formRow publishBtn">
			  <a href="<?= base_url('issues/rejected_issue_flag/'.$key->issue_id)?>" class="reject">Delete</a>
			  <button class="approve" data-id="<?php echo $key->issue_id?>" >Ignore</button>
			</div>
            </form>

          </div>

      </div>
<?php 
		   if($res_flag){?>
 <div class="ciWrapp">
		<h2>Reasons</h2>
           <?php 
		   foreach($res_flag as $res_flag){?>      
		<span><label><?php echo  $res_flag->reason?></label></span>
<?php }?>   
		
	</div>
	<?php }?>   
    </section> <!-- contentContainer -->
<?php }
                 echo"<center>";
				 echo $links; 
				   echo"</center>";
}else{
	
	echo "No Issuse Found";
}?>

	<?php require_once('mainfooter.php');?>