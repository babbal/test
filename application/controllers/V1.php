<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class v1 extends CI_Controller {
	
			function __construct()
			{
				parent::__construct();
				$this->load->library('encrypt');
				$this->load->helper('response');
				$this->load->helper('cookie');
				$this->load->model('SignupModel');
				$this->load->model('SignInModel');
				$this->load->library("pagination");
				define('AUTH','123456');
			}
			public function test(){
				echo "test";
				exit;
			}
	        public function register(){

				$username=$this->input->post('username');
				$first_name=$this->input->post('first_name');
				$last_name=$this->input->post('last_name');
				$email=$this->input->post('email');
				$phone=$this->input->post('phone');
				$location=$this->input->post('location');
				$province=$this->input->post('province');
				$profile_image=$this->input->post('profile_image');
				$social_media_id=$this->input->post('social_media_id');
				$device_token=$this->input->post('device_token');
				$device_type=$this->input->post('device_type');
				$sm_type=$this->input->post('sm_type');
				
				
				$response = array();
				if ($device_token==''){
					$response["error"] = true;
					$response["message"] =  'Device Token Required';
					EchoResponse(200, $response);

				}elseif ($device_type==''){
					$response["error"] = true;
					$response["message"] =  'Device Type Required';
					EchoResponse(200, $response);
                }elseif($social_media_id==''){
							$response["error"] = true;
							$response["message"] =  'Social Media Id Required';
							EchoResponse(200, $response);
				}elseif ($sm_type==''){
							$response["error"] = true;
							$response["message"] =  'Social Media Type Required';
							EchoResponse(200, $response);

				}else{
					if($sm_type =="facebook" OR $sm_type =="twitter" OR $sm_type =="google"){
						
							$check = array(
								'social_media_id'		=>$social_media_id,
								);

							$user=$this->SignInModel->user_exist($check);

							if($user){
								$data = array(
									'device_token'=>$device_token,
									'device_type'=>$device_type
									);	

								$this->db->where('user_id',$user->user_id);		 
								$query=$this->db->update('users',$data);
								
								$response["error"] = false;
								$response["message"] ='Login Successfully';
								$response["data"]["id"]= $user->user_id;
								$response["data"]["username"]= $user->username;
								$response["data"]["firstname"]= $user->first_name;
								$response["data"]["lastname"]= $user->last_name;
								$response["data"]["email"]= $user->email;
								$response["data"]["phone"]= $user->phone;
								$response["data"]["social_media_id"]= $user->social_media_id;
								$response["data"]["device_token"]= $device_token;
								$response["data"]["device_type"]= $device_type;
								$response["data"]["sm_type"]= $sm_type;
								$response["data"]["time"]= $user->created_at;
								EchoResponse(200, $response);
							}else{

								$data = array(
									'username'=>$username,
									'first_name'=>$first_name,
									'last_name'=>$last_name,
									'email'=>$email,
									'phone'=>$phone,
									'social_media_id'=>$social_media_id,
									'sm_type'=>$sm_type,
									'profile_image'=>$profile_image,
									'device_token'=>$device_token,
									'device_type'=>$device_type,
									'location'=>$location,
									'province'=>$province,
									'status'=>1,
									'user_type'=>2
									);	

								$record=$this->SignupModel->insert_userSetting($data);	
								if($record->user_id > 0){
									$response["error"] = false;
									$response["message"] ='Signup Successfully';
									$response["data"]["id"]= $record->user_id;
									$response["data"]["username"]= $record->username;
									$response["data"]["firstname"]= $record->first_name;
									$response["data"]["lastname"]= $record->last_name;
									$response["data"]["email"]= $record->email;
									$response["data"]["phone"]= $record->phone;
									$response["data"]["social_media_id"]= $record->social_media_id;
									$response["data"]["device_token"]= $record->device_token;
									$response["data"]["device_type"]= $record->device_type;
									$response["data"]["sm_type"]= $record->sm_type;
									$response["data"]["time"]= $record->created_at;
									EchoResponse(200, $response);
                                }else{
									$response["error"]   = true;
									$response["message"] = 'Failed to create user';
									EchoResponse(200, $response);

								}



							}


						

					}else{
						        
                            $response["error"]   = true;
							$response["message"] = 'Social media type not correct';
							EchoResponse(200, $response);



					}


				}

            }
			public function update_profile(){
						
						$username=$this->input->post('username');
						$first_name=$this->input->post('first_name');
						$last_name=$this->input->post('last_name');
						$user_id=$this->input->post('user_id');
						$email=$this->input->post('email');
						$location=$this->input->post('location');
						$province=$this->input->post('province');
						$phone=$this->input->post('phone');
						$this->db->select('*');
						$this->db->where('user_id', $user_id);
						$this->db->from('users');
						$rec = $this->db->get()->result();
						if(empty($rec)){
							$response["error"] = true;
							$response["message"] =  'No such users found';
							EchoResponse(200, $response); 
						}elseif($user_id==''){
							$response["error"] = true;
							$response["message"] =  'User Id Required';
							EchoResponse(200, $response); 
						}else{

							if (empty($_FILES["profile_image"]["name"])){
								$profile_image=@$rec[0]->profile_image;
							}else{

								$temp = explode(".", $_FILES["profile_image"]["name"]);
								$extension = end($temp);
								$path="./assets/user_images/";

								$filename = basename($_FILES["profile_image"]["name"]);
						 
								 $filename = time().".".$extension; 
								 if(move_uploaded_file($_FILES["profile_image"]["tmp_name"],$path . $filename))
								 {

									$profile_image=base_url()."assets/user_images/".$filename;

								 }else{
									$profile_image=@$rec[0]->profile_image;
								 }


							}
						
						
						if($location){
							$location=$location;
						}else{
							$location=@$rec[0]->location;
						}
						if($username){
							$username=$username;
						}else{
							$username=@$rec[0]->username;
						}
						if($first_name){
							$first_name=$first_name;
						}else{
							$first_name=@$rec[0]->first_name;
						}
						if($last_name){
							$last_name=$last_name;
						}else{
							$last_name=@$rec[0]->last_name;
						}
						if($email){
							$email=$email;
						}else{
							$email=@$rec[0]->email;
						}
						if($phone){
							$phone=$phone;
						}else{
							$phone=@$rec[0]->phone;
						}
						if($province){
							$province=$province;
						}else{
							$province=@$rec[0]->province;
						}
						
						$data = array(
							'username'    =>$username,
							'first_name'    =>$first_name,
							'last_name'    =>$last_name,
							'profile_image' =>$profile_image,
							'email' =>$email,
							'location'    =>$location,
							'phone'    =>$phone,
							'province'    =>$province,
							);

						$this->db->where('user_id',$user_id);
						$query=$this->db->update('users',$data);
						if($query){

							$response["error"] = false;
							$response["message"] ='Updated Successfully';
							EchoResponse(200, $response);
						}else{
							$response["error"] = true;
							$response["message"] =  'Failed to update';
							EchoResponse(200, $response);
						}




					  }
	        }
	        public function head_of_province(){
				
				$province=$this->input->post('province');
				if($province==''){
					$response["error"] = true;
					$response["message"] =  'Province name Required';
					EchoResponse(200, $response);
				}else{
					$this->db->select('*');
					$this->db->where('province', $province);
					$this->db->from('head_of_province');
					$data = $this->db->get()->result();
					if(!empty($data)){
						$response["error"] = false;
						$response["message"] =  'Head of Province';
						$response["data"] = $data;
						EchoResponse(200, $response);
						
					}else{
						$response["error"] = true;
						$response["message"] =  'No data found';
						EchoResponse(200, $response);
					}
					
					
				}
	
			
	        }
			public function record_case($section_number,$section_type,$table) {
							
							$this->db->select('case_id');
							$this->db->where('section_key', $section_number);
							$this->db->where('section_type', $section_type);
							$this->db->from($table);
							$num_results = $this->db->get()->result();
							return count($num_results);
					
			}
			public function fetch_case($limit,$offset,$section_number,$section_type,$table) {
				
				$this->db->select('case_id,case_title,section_type');
				$this->db->where('section_key', $section_number);
				$this->db->where('section_type', $section_type);
				$this->db->from($table);
				$this->db->limit($limit, $offset);
				//$this->db->order_by('case_id','asc');
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {

						$data[] = $row;
					}
					return $data;
				}
				return false;
				
				
			}
			public function get_section(){
						
						$header=$this->input->get_request_header('header_law');
						$section_number=$this->input->post('section_number');
						$section_type=$this->input->post('section_type');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($section_number==''){
							$response["error"] = true;
							$response["message"] =  'Section Number Required';
							EchoResponse(200, $response);
						}elseif($section_type==''){
							$response["error"] = true;
							$response["message"] =  'Section Type Required';
							EchoResponse(200, $response);
						}else{
							if($section_type =="PPC" OR $section_type =="QSO" OR $section_type =="CRPC" OR $section_type =="CPC"){
								$this->db->select('section_id,section_number,section_title,section_description,section_type');
								$this->db->where('section_number', $section_number);
								$this->db->where('section_type', $section_type);
								$this->db->from('sections');
								$data = $this->db->get()->result();
								
								$section_data=array();
								$section_data=$data;
									if(!empty($data)){
										$table=$section_type."_cases";
										$limit=3;
										$page = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
										$offset=$page*$limit;
										$total_rows = $this->record_case($section_number,$section_type,$table);
										$cases= $this->fetch_case($limit,$offset,$section_number,$section_type,$table);
										$total_pages= ceil($total_rows / $limit);
										
										if(!empty($cases)){
											
													$cases_data=array();
													foreach($cases as $cases){
														
															$cases_data[] = array(
																"case_id" => $cases->case_id,
																"case_title" => $cases->case_title,
																);

														}
													$response["error"] = false;
													$response["message"] =  'Sections';
													$response["section"] = $data;
													$response['cases'] = $cases_data;
													$response["total_pages"] = $total_pages;
													EchoResponse(200, $response);
										}else{
											
													$cases_data=array();
													$response["error"] = false;
													$response["message"] =  'Sections';
													$response["section"] = $data;
													$response['cases'] = $cases_data;
													EchoResponse(200, $response);
										}
										
										
									}else{
										$response["error"] = true;
										$response["message"] =  'No data found';
										EchoResponse(200, $response);
									}
							}else{
								$response["error"] = true;
								$response["message"] =  'Wrong Section Type';
								EchoResponse(200, $response);
							}
							
						}
	
			
	        }	
			public function get_case_detail(){
						
						$header=$this->input->get_request_header('header_law');
						$case_id=$this->input->post('case_id');
						$section_type=$this->input->post('section_type');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($case_id==''){
							$response["error"] = true;
							$response["message"] =  'Case Id Required';
							EchoResponse(200, $response);
						}elseif($section_type==''){
							$response["error"] = true;
							$response["message"] =  'Section Type Required';
							EchoResponse(200, $response);
						}else{
							if($section_type =="PPC" OR $section_type =="QSO" OR $section_type =="CRPC" OR $section_type =="CPC"){
										$table=$section_type."_cases";
										$this->db->select('case_id,case_title,case_description');
										$this->db->where('case_id', $case_id);
										//$this->db->where('section_type', $section_type);
										$this->db->from($table);
										$data = $this->db->get()->result();
										if(!empty($data)){
												$response["error"] = false;
												$response["message"] =  'Case Of Law';
												$response["case"] = $data;
												EchoResponse(200, $response);
													
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
							}else{
										$response["error"] = true;
										$response["message"] =  'Wrong Section Type';
										EchoResponse(200, $response);
							}
							
						}
	
			
	        }
			public function record_topic($parent_id) {
							
							$this->db->select('topic_id');
							$this->db->where('parent_id', $parent_id);
							$this->db->from('topics');
							$num_results = $this->db->get()->result();
							return count($num_results);
					
			}
			public function fetch_topic($limit,$offset,$parent_id) {
				
				$this->db->select('topic_id,topic_name');
				$this->db->where('parent_id', $parent_id);
				$this->db->from('topics');
				$this->db->limit($limit, $offset);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {

						$data[] = $row;
					}
					return $data;
				}
				return false;
				
				
			}
			public function get_topic(){
						
						$header=$this->input->get_request_header('header_law');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}else{
							
										$limit=3;
										$page = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
										$parent_id = $this->input->post('parent_id') ? $this->input->post('parent_id') : 0;
										$offset=$page*$limit;
										$total_rows = $this->record_topic($parent_id);
										$record= $this->fetch_topic($limit,$offset,$parent_id);
										$total_pages= ceil($total_rows / $limit);
										if(!empty($record)){
												$response["error"] = false;
												$response["message"] =  'Case Of Law';
												$response["topic"] = $record;
												$response["total_pages"] = $total_pages;
												EchoResponse(200, $response);
													
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
							
							
						}
			
					
			}
			public function record_topic_list($topic_id) {
							
							$this->db->select('case_id');
							$this->db->where('section_key', $topic_id);
							$this->db->from('topic_cases');
							$num_results = $this->db->get()->result();
							return count($num_results);
					
			}
			public function fetch_topic_list($limit,$offset,$topic_id) {
				
				$this->db->select('case_id,case_title');
				$this->db->where('section_key', $topic_id);
				$this->db->from('topic_cases');
				$this->db->limit($limit, $offset);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {

						$data[] = $row;
					}
					return $data;
				}
				return false;
				
				
			}
			public function topic_list(){
						
						$header=$this->input->get_request_header('header_law');
						$topic_id=$this->input->post('topic_id');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($topic_id==''){
							$response["error"] = true;
							$response["message"] =  'Topic Id Required';
							EchoResponse(200, $response);
						}else{
							            $limit=2;
										$page = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
										$offset=$page*$limit;
										$total_rows = $this->record_topic_list($topic_id);
										$record= $this->fetch_topic_list($limit,$offset,$topic_id);
										$total_pages= ceil($total_rows / $limit);
										
										if(!empty($record)){
												$cases_data=array();
													foreach($record as $data){
														
															$cases_data[] = array(
																"case_id" => $data->case_id,
																"case_title" => $data->case_title,
																
																);

														}
													$response["error"] = false;
													$response["message"] =  'Case of Law';
													$response['cases'] = $cases_data;
													$response["total_pages"] = $total_pages;
													EchoResponse(200, $response);
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
							
							
						}
	
			
	        }
			public function get_case_detail_topic(){
						
						$header=$this->input->get_request_header('header_law');
						$case_id=$this->input->post('case_id');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($case_id==''){
							$response["error"] = true;
							$response["message"] =  'Case Id Required';
							EchoResponse(200, $response);
						}else{
							
										$table="topic_cases";
										$this->db->select('case_id,case_title,case_description');
										$this->db->where('case_id', $case_id);
										$this->db->from($table);
										$data = $this->db->get()->result();
										if(!empty($data)){
												$response["error"] = false;
												$response["message"] =  'Case Of Law';
												$response["case"] = $data;
												EchoResponse(200, $response);
													
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
							
							
						}
	
			
	        }
			public function get_act(){
				$header=$this->input->get_request_header('header_law');
				$province=$this->input->post('province');
				
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($province==''){
							$response["error"] = true;
							$response["message"] =  'Province name Required';
							EchoResponse(200, $response);
						}else{
							            $table=$province."_acts";
										$this->db->select('*');
										$this->db->order_by("act_year", "desc");
										$this->db->from($table);
										$data = $this->db->get()->result();
										
											if(!empty($data)){
													$acts=array();
													foreach($data as $data){
														$link=base_url()."assets/".$table."/".$data->link;
															$acts[] = array(
																"act_id" => $data->act_id,
																"act_title" => $data->act_title,
																"act_year" => $data->act_year,
																"link" => $link,
																);

														}
														$response["error"] = false;
														$response["message"] =  'Acts';
														$response['acts'] = $acts;
														EchoResponse(200, $response);		
														
											}else{
													$response["error"] = true;
													$response["message"] =  'No data found';
													EchoResponse(200, $response);
											}
						}
			}
			
			public function record_citation($year=NULL,$page_no=NULL,$book_name=NULL,$court_name=NULL,$case_title=NULL,$method=NULL) {
				if($method==1){
						$this->db->select('case_id');
						if($year){
						  $this->db->where('year', $year);
						}
						if($page_no){
						  $this->db->where('page_no', $page_no);
						}
						if($court_name){
						  $this->db->like('court_name', $court_name);
						}
						if($book_name){
						  $this->db->where('book_name', $book_name);
						}
						$this->db->from('cases');
						$num_results = $this->db->get()->result();
						return count($num_results);
				}elseif($method==2){
							    $this->db->select('case_id');
								$this->db->like('court_name', $court_name);
								$this->db->from('cases');
								$num_results = $this->db->get()->result();
								return count($num_results);
				}else{
					            $this->db->select('case_id');
								$this->db->like('case_title', $case_title);
								$this->db->from('cases');
								$num_results = $this->db->get()->result();
								return count($num_results);
				}
			}
			public function fetch_citation($limit=NULL,$offset=NULL,$year=NULL,$page_no=NULL,$book_name=NULL,$court_name=NULL,$case_title=NULL,$method=NULL) {
				if($method == 1){
					                            $this->db->select('case_id,case_title');
												if($year){
												  $this->db->where('year', $year);
												}
												if($page_no){
												  $this->db->where('page_no', $page_no);
												}
												if($court_name){
												  $this->db->like('court_name', $court_name);
												}
												if($book_name){
												  $this->db->where('book_name', $book_name);
												}
												$this->db->from('cases');
												$this->db->limit($limit, $offset);
												$query = $this->db->get();
												if ($query->num_rows() > 0) {
													foreach ($query->result() as $row) {
                                                      $data[] = $row;
													}
													return $data;
												}
				}elseif($method == 2){
					                            $this->db->select('case_id,case_title');
												$this->db->like('court_name', $court_name);
												$this->db->from('cases');
												$this->db->limit($limit, $offset);
												$query = $this->db->get();
												if ($query->num_rows() > 0) {
													foreach ($query->result() as $row) {
                                                         $data[] = $row;
													}
													return $data;
												}
				}else{
					                            $this->db->select('case_id,case_title');
												$this->db->like('case_title', $case_title);
												$this->db->from('cases');
												$this->db->limit($limit, $offset);
												$query = $this->db->get();
												if ($query->num_rows() > 0) {
													foreach ($query->result() as $row) {
                                                          $data[] = $row;
													}
													return $data;
												}
				}
				return false;
			}
			public function get_citation(){
						$header=$this->input->get_request_header('header_law');
						$method=$this->input->post('method');
						$book_name=$this->input->post('book_name');
						$court_name=$this->input->post('court_name');
						$case_title=$this->input->post('case_title');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($method ==''){
							$response["error"] = true;
							$response["message"] =  'Please Method';
							EchoResponse(200, $response);
						}else{
										if($method==1){
											          if(empty($book_name)){
															$response["error"] = true;
															$response["message"] =  'Please Enter Book Name';
															EchoResponse(200, $response);
														}else{
															$limit=2;
															$page_no = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
															$year = $this->input->post('year') ? $this->input->post('year') : "";
															$page = $this->input->post('page') ? $this->input->post('page') : "";
															$court_name = $this->input->post('court_name') ? $this->input->post('court_name') : "";
															$offset=$page_no*$limit;
								$total_rows = $this->record_citation($year,$page,$book_name,$court_name,$case_title,$method);
								$record= $this->fetch_citation($limit,$offset,$year,$page,$book_name,$court_name,$case_title,$method);
															$total_pages= ceil($total_rows / $limit);
															   if(!empty($record)){
																	$cases_data=array();
																		foreach($record as $data){
																			 $cases_data[] = array(
																					"case_id" => $data->case_id,
																					"case_title" => $data->case_title,
																					);
                                                                        }
																		$response["error"] = false;
																		$response["message"] =  'Case of Law';
																		$response['cases'] = $cases_data;
																		$response["total_pages"] = $total_pages;
																		EchoResponse(200, $response);
																}else{
																	$response["error"] = true;
																	$response["message"] =  'No data found';
																	EchoResponse(200, $response);
																}
														}
										}elseif($method==2){
													    if(empty($court_name)){
															$response["error"] = true;
															$response["message"] =  'Please Enter Court Name';
															EchoResponse(200, $response);
														}else{
															$limit=2;
															$page_no = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
															$year = $this->input->post('year') ? $this->input->post('year') : "";
															$page = $this->input->post('page') ? $this->input->post('page') : "";
															$court_name = $this->input->post('court_name') ? $this->input->post('court_name') : "";
															$offset=$page_no*$limit;
								$total_rows = $this->record_citation($year,$page,$book_name,$court_name,$case_title,$method);
								$record= $this->fetch_citation($limit,$offset,$year,$page,$book_name,$court_name,$case_title,$method);
															$total_pages= ceil($total_rows / $limit);
															   if(!empty($record)){
																	$cases_data=array();
																		foreach($record as $data){
																			 $cases_data[] = array(
																					"case_id" => $data->case_id,
																					"case_title" => $data->case_title,
																					);
                                                                            }
																		$response["error"] = false;
																		$response["message"] =  'Case of Law';
																		$response['cases'] = $cases_data;
																		$response["total_pages"] = $total_pages;
																		EchoResponse(200, $response);
																}else{
																	$response["error"] = true;
																	$response["message"] =  'No data found';
																	EchoResponse(200, $response);
																}
														}
										}else{
											            if(empty($case_title)){
															$response["error"] = true;
															$response["message"] =  'Please Enter Case Title';
															EchoResponse(200, $response);
														}else{
															$limit=2;
															$page_no = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
															$year = $this->input->post('year') ? $this->input->post('year') : "";
															$page = $this->input->post('page') ? $this->input->post('page') : "";
															$court_name = $this->input->post('court_name') ? $this->input->post('court_name') : "";
															$offset=$page_no*$limit;
								$total_rows = $this->record_citation($year,$page,$book_name,$court_name,$case_title,$method);
								$record= $this->fetch_citation($limit,$offset,$year,$page,$book_name,$court_name,$case_title,$method);
															$total_pages= ceil($total_rows / $limit);
															   if(!empty($record)){
																	$cases_data=array();
																		foreach($record as $data){
																			 $cases_data[] = array(
																					"case_id" => $data->case_id,
																					"case_title" => $data->case_title,
																					);
                                                                        }
																		$response["error"] = false;
																		$response["message"] =  'Case of Law';
																		$response['cases'] = $cases_data;
																		$response["total_pages"] = $total_pages;
																		EchoResponse(200, $response);
																}else{
																	$response["error"] = true;
																	$response["message"] =  'No data found';
																	EchoResponse(200, $response);
																}}	} } }
			public function get_case_detail_citation(){
						$header=$this->input->get_request_header('header_law');
						$case_id=$this->input->post('case_id');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($case_id==''){
							$response["error"] = true;
							$response["message"] =  'Case Id Required';
							EchoResponse(200, $response);
						}else{
							            $table="cases";
										$this->db->select('case_id,case_title,case_description');
										$this->db->where('case_id', $case_id);
										$this->db->from($table);
										$data = $this->db->get()->result();
										if(!empty($data)){
												$response["error"] = false;
												$response["message"] =  'Case Of Law';
												$response["case"] = $data;
												EchoResponse(200, $response);
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
						}
	        }
			public function record_video() {
							$this->db->select('video_id');
							$this->db->from('videos');
							$num_results = $this->db->get()->result();
							return count($num_results);
			}
			public function fetch_video($limit,$offset) {
				$this->db->select('video_id,video_title,video_url,video_image');
				$this->db->from('videos');
				$this->db->limit($limit, $offset);
				$query = $this->db->get();
				if ($query->num_rows() > 0) {
					foreach ($query->result() as $row) {
                          $data[] = $row;
					}
					return $data;
				}
				return false;
			}
			public function get_videos(){
						$header=$this->input->get_request_header('header_law');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}else{
							            $limit=2;
										$page_no = $this->input->post('page_no') ? $this->input->post('page_no') : 0;
										$offset=$page_no*$limit;
										$total_rows = $this->record_video();
										$record= $this->fetch_video($limit,$offset);
										$total_pages= ceil($total_rows / $limit);
										if(!empty($record)){
												$videos=array();
													foreach($record as $data){
														$videos[] = array(
																"video_id" => $data->video_id,
																"video_title" => $data->video_title,
																"video_url" => $data->video_url,
																"video_image" => $data->video_image,
																);

														}
													$response["error"] = false;
													$response["message"] =  'Videos';
													$response['videos'] = $videos;
													$response["total_pages"] = $total_pages;
													EchoResponse(200, $response);
											}else{
												$response["error"] = true;
												$response["message"] =  'No data found';
												EchoResponse(200, $response);
											}
						}
	        }
			public function get_constitution(){
						$header=$this->input->get_request_header('header_law');
						$section_number=$this->input->post('section_number');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}elseif($section_number==''){
							$response["error"] = true;
							$response["message"] =  'Section Number Required';
							EchoResponse(200, $response);
						}else{
								$this->db->select('con_title,con_description,con_amendment');
								$this->db->where('section_number', $section_number);
								$this->db->from('constitution');
								$data = $this->db->get()->result();
								if(!empty($data)){
										$cases_data=array();
										$response["error"] = false;
										$response["message"] =  'Constitution';
										$response["constitution"] = $data;
										EchoResponse(200, $response);
								}else{
										$response["error"] = true;
										$response["message"] =  'No data found';
										EchoResponse(200, $response);
								}
						}
	                     }
			public function thanksto(){
						$header=$this->input->get_request_header('header_law');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}else{
								$this->db->select('name,image');
								$this->db->from('thanks');
								$data = $this->db->get()->result();
								if(!empty($data)){
										$cases_data=array();
										$response["error"] = false;
										$response["message"] =  'thanks';
										$response["thanks"] = $data;
										EchoResponse(200, $response);
								}else{
										$response["error"] = true;
										$response["message"] =  'No data found';
										EchoResponse(200, $response);
								}
						}
	}
			public function get_detail_thanksto(){
						$header=$this->input->get_request_header('header_law');
						$id=$this->input->post('id');
						if($header==''){
							$response["error"] = true;
							$response["message"] =  'Header Value Required';
							EchoResponse(200, $response);
						}elseif($header !=AUTH){
							$response["error"] = true;
							$response["message"] =  'Header Value Wrong';
							EchoResponse(200, $response);
						}else{
								$this->db->select('*');
								$this->db->from('thanks');
								$this->db->where('id',$id);
								$data = $this->db->get()->result();
								if(!empty($data)){
										$cases_data=array();
										$response["error"] = false;
										$response["message"] =  'thanks';
										$response["thanks"] = $data;
										EchoResponse(200, $response);
								}else{
										$response["error"] = true;
										$response["message"] =  'No data found';
										EchoResponse(200, $response);
								}
						}
	                    }
}
?>